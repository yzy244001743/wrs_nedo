import cv2
import os
import trimesh
import trimesh.sample as sample
import math
import yaml
import numpy as np
from cv2 import aruco
from sklearn import cluster
import utiltools.robotmath as rm
from pandaplotutils import pandactrl
from vision.camcalib import arucotrack_fusion as af
import motionplanning.collisioncheckerball as cdball
import pickle
from environment import collisionmodel as cm
from panda3d.core import *
from direct.gui.DirectGui import *
import copy

class UR3DDTCtl(object):
    """
    the controller module of the application

    author: weiwei
    date: 20180925
    """

    def __init__(self, camnplist, caminfolist):
        """

        :param camnplist: [camnodepath]
        :param caminfolist: [[[w,h], pandamat4, mtx], ...]

        author: weiwei
        date: 20190428
        """

        self.camnplist = camnplist
        self.caminfolist = caminfolist
        self.framenplist = [[]]
        self.boxcmlist =[[]]
        self.fixedframenplist = [[]]
        self.fixedboxcmlist =[[]]
        self.stackid = [[]]
        self.fixedstackid = [[]]

    def dellastcaptured(self):
        if len(self.framenplist[0]) > 0:
            self.framenplist[0][-1].removeNode()
            self.framenplist[0].pop()

        if len(self.boxcmlist[0]) > 0:
            self.boxcmlist[0][-1].removeNode()
            self.boxcmlist[0].pop()

        if len(self.fixedframenplist[0]) > 0:
            self.fixedframenplist[0][-1].removeNode()
            self.fixedframenplist[0].pop()

        if len(self.fixedboxcmlist[0]) > 0:
            self.fixedboxcmlist[0][-1].removeNode()
            self.fixedboxcmlist[0].pop()

        if len(self.fixedstackid[0]) > 0:
            print(type(self.fixedstackid[0][-1]))
            self.fixedstackid[0].pop()

    def capture(self):
        self.fixedframenplist[0].extend(copy.deepcopy(self.framenplist[0]))
        self.fixedboxcmlist[0].extend(copy.deepcopy(self.boxcmlist[0]))
        self.fixedstackid[0].extend(copy.deepcopy(self.stackid[0]))
        self.stackid[0] = []

        if len(self.framenplist[0]) > 0:
            for axisnp in self.framenplist[0]:
                axisnp.removeNode()
            self.framenplist[0] = []

        if len(self.boxcmlist[0]) > 0:
            for boxcm in self.boxcmlist[0]:
                boxcm.removeNode()
            self.boxcmlist[0] = []

    def restart(self):
        if len(self.framenplist[0]) > 0:
            for axisnp in self.framenplist[0]:
                axisnp.removeNode()
            self.framenplist[0] = []

        if len(self.boxcmlist[0]) > 0:
            for boxcm in self.boxcmlist[0]:
                boxcm.removeNode()
            self.boxcmlist[0] = []

        if len(self.fixedframenplist[0]) > 0:
            for axisnp in self.fixedframenplist[0]:
                axisnp.removeNode()
            self.fixedframenplist[0] = []

        if len(self.fixedboxcmlist[0]) > 0:
            for boxcm in self.fixedboxcmlist[0]:
                boxcm.removeNode()
            self.fixedboxcmlist[0] = []

        self.framenplist[0] =[]
        self.boxcmlist[0] = []
        self.fixedframenplist[0] = []
        self.fixedboxcmlist[0] = []
        self.stackid[0] = []
        self.fixedstackid[0] = []

class HLabGUI(object):
    """
    the graphical user interface of the application

    author: weiwei
    date: 20180925
    """

    def captureFromTeacher(self):
        lastnposes = len(self.scctrl.fixedframenplist[0])
        self.scctrl.capture()
        self.nposes = len(self.scctrl.fixedframenplist[0])
        self.textNPose['text']='#Poses: '+str(self.nposes)
        if self.nposes == 0:
            self.textCaptured['text']='Failed to capture!'
        else:
            self.textCaptured['text']=str(self.nposes-lastnposes)+' poses newly captured!'

    def deleteCapture(self):
        self.scctrl.dellastcaptured()
        self.nposes = len(self.scctrl.fixedframenplist[0])
        self.textNPose['text']='#Poses: '+str(self.nposes)
        if self.nposes == 0:
            self.textCaptured['text']='All poses are deleted!'
        else:
            self.textCaptured['text']='One pose deleted!\n'+str(self.nposes)+' remain!'

    def execplan(self):
        self.textCaptured['text']='Planning!'
        self.scctrl.plannplay()

    def restart(self):
        self.scctrl.restart()
        self.nposes = len(self.scctrl.fixedframenplist[0])
        self.textNPose['text']='#Poses: '+str(self.nposes)
        self.textCaptured['text']='Ready to capture'

    def __init__(self, scenarioctrl):
        self.scctrl = scenarioctrl
        this_dir, this_filename = os.path.split(__file__)
        self.imageObject = OnscreenImage(image="./gui/banner250x1080.png", pos=(1.55, 0, 0), scale=(250/1080.0,1,1))

        bcmappath = Filename.fromOsSpecific(os.path.join(this_dir, "gui", "buttoncapture_maps.egg"))
        maps = loader.loadModel(bcmappath)
        self.capturebtn = DirectButton(frameSize=(-1,1,-.25,.25), geom=(maps.find('**/buttoncapture_ready'),
                               maps.find('**/buttoncapture_click'),
                               maps.find('**/buttoncapture_rollover')),
                               pos=(1.45, 0, .54), scale=(.06,.12,.12),
                               command = self.captureFromTeacher)

        brmappath = Filename.fromOsSpecific(os.path.join(this_dir, "gui", "buttondelete_maps.egg"))
        maps = loader.loadModel(brmappath)
        self.runbtn = DirectButton(frameSize=(-1,1,-.25,.25), geom=(maps.find('**/buttondelete_ready'),
                               maps.find('**/buttondelete_click'),
                               maps.find('**/buttondelete_rollover')),
                               pos=(1.575, 0, .54), scale=(.06,.12,.12),
                               command = self.deleteCapture)

        brmappath = Filename.fromOsSpecific(os.path.join(this_dir, "gui", "buttonrestart_maps.egg"))
        maps = loader.loadModel(brmappath)
        self.runbtn = DirectButton(frameSize=(-1,1,-.25,.25), geom=(maps.find('**/buttonrestart_ready'),
                               maps.find('**/buttonrestart_click'),
                               maps.find('**/buttonrestart_rollover')),
                               pos=(1.7, 0, .54), scale=(.06,.12,.12),
                               command = self.restart)

        brmappath = Filename.fromOsSpecific(os.path.join(this_dir, "gui", "buttonplan_maps.egg"))
        maps = loader.loadModel(brmappath)
        self.runbtn = DirectButton(frameSize=(-1,1,-.25,.25), geom=(maps.find('**/buttonplan_ready'),
                               maps.find('**/buttonplan_click'),
                               maps.find('**/buttonplan_rollover')),
                               pos=(1.45, 0, .47), scale=(.06,.12,.12),
                               command = self.execplan)

        self.nposes = 0
        self.textNPose = OnscreenText(text='#Poses: '+str(self.nposes), pos=(1.4, -.85, 0), scale=0.03, fg=(1., 1., 1., 1),
                                      align=TextNode.ALeft, mayChange=1)
        self.textCaptured = OnscreenText(text='Ready to capture', pos=(1.4, -.9, 0), scale=0.03, fg=(1., 1., 1., 1),
                                      align=TextNode.ALeft, mayChange=1)

        # setup cameras
        scale = 150
        self.camshowlist = [] # camshowlist = [[texture, cardnp]]
        for i, caminfo in enumerate(self.scctrl.caminfolist):
            imgw, imgh = caminfo[0]
            cm = CardMaker('card')
            offrot = np.dot(rm.rodrigues([0,0,1], 90), rm.rodrigues([1,0,0], 90))
            offpos = np.array([-scale*imgw/imgh/2, -scale/2, self.scctrl.caminfolist[i][2][0,0]*scale/imgw])
            cardmat = base.pg.npToMat4(offrot, offpos)*self.scctrl.caminfolist[i][1]
            cardnp = base.render.attachNewNode(cm.generate())
            cardnp.setMat(cardmat)
            self.camshowlist.append([Texture(), cardnp])
            self.camshowlist[i][0].setup2dTexture(int(imgh), int(imgw), Texture.T_unsigned_byte, Texture.F_luminance)
            self.camshowlist[i][1].setScale(scale, scale, scale*imgw / imgh)

def getimglist(camcaps, cammtxs, camdists, camrelhomos,frameavglist):
    imglist = []
    for i in range(len(camcaps)):
        imgi = camcaps[i].read()[1]

        if imgi is not None:
            if i == 0:
                for id in frameavglist:
                    posvecavg = frameavglist[id][0]
                    rotmatavg,_ = cv2.Rodrigues(frameavglist[id][1])
                    aruco.drawAxis(imgi, cammtxs[i], camdists[i], rotmatavg, posvecavg/1000.0, 0.1)
            elif i == 1:
                for id in frameavglist:
                    posvecavg = frameavglist[id][0]
                    matinb = np.dot(camrelhomos[i-1], rm.homobuild(posvecavg, frameavglist[id][1]))
                    rotmatavg,_ = cv2.Rodrigues(matinb[:3,:3])
                    posvecavg = matinb[:3,3]
                    aruco.drawAxis(imgi, cammtxs[i], camdists[i], rotmatavg, posvecavg/1000.0, 0.1)
            elif i == 2:
                for id in frameavglist:
                    posvecavg = frameavglist[id][0]
                    matinb = np.dot(camrelhomos[i-1], rm.homobuild(posvecavg, frameavglist[id][1]))
                    rotmatavg,_ = cv2.Rodrigues(matinb[:3,:3])
                    posvecavg = matinb[:3,3]
                    aruco.drawAxis(imgi, cammtxs[i], camdists[i], rotmatavg, posvecavg/1000.0, 0.1)
        imglist.append(imgi)
    return imglist

def loadobj(name):
    this_dir, this_filename = os.path.split(__file__)
    objpath = os.path.join(this_dir, "objects", name)
    objtrimesh = trimesh.load_mesh(objpath)
    # assume 40mm diameter
    evensamples = sample.sample_surface_even(objtrimesh, int(math.ceil(objtrimesh.area/200.0)))
    objnp = base.pg.loadstlaspandanp_fn(objpath)
    cdobjnp = objnp.attachNewNode(cdball.getcdboxcn(objnp))
    cdobjnp.setTag('boxcd', 'true')
    objnp.setTag('samples', pickle.dumps(evensamples))
    return [objnp, cdobjnp, objpath]

def dispobject(base, objnp, posvecavg, rotmatavg):
    mtx = np.eye(4)
    mtx[:3,:3] = rotmatavg
    mtx[:3,3] = posvecavg
    print(mtx)
    obsrotmat1 = base.pg.np4ToMat4(mtx)
    objnp.setMat(obsrotmat1)
    objnp.setColor(.3, .3, .6, 1)
    objnp.reparentTo(base.render)


if __name__=='__main__':
    base = pandactrl.World(camp=[500, 500, -2000], lookatp=[0, 0, 100], up=[0,0,-1], fov = 40, w = 1920, h = 1080)

    cam0np = base.pggen.genAxis()
    cam0np.reparentTo(base.render)

    homo_rb20 = yaml.load(open('../vision/camcalib/homo_rb20.yaml', 'r'))
    pandamat4homo_r2 = base.pg.np4ToMat4(rm.homoinverse(homo_rb20))
    cam2np = base.pggen.genAxis(spos = pandamat4homo_r2.getRow3(3), pandamat3 = pandamat4homo_r2.getUpper3())
    cam2np.reparentTo(base.render)

    homo_rb40 = yaml.load(open('../vision/camcalib/homo_rb40.yaml', 'r'))
    pandamat4homo_r4 = base.pg.np4ToMat4(rm.homoinverse(homo_rb40))
    cam4np = base.pggen.genAxis(spos = pandamat4homo_r4.getRow3(3), pandamat3 = pandamat4homo_r4.getUpper3())
    cam4np.reparentTo(base.render)

    # show in videos
    mtx0, dist0, rvecs0, tvecs0, candfiles0 = yaml.load(open('../vision/camcalib/cam0_calib.yaml', 'r'))
    mtx2, dist2, rvecs2, tvecs2, candfiles2 = yaml.load(open('../vision/camcalib/cam2_calib.yaml', 'r'))
    mtx4, dist4, rvecs4, tvecs4, candfiles4 = yaml.load(open('../vision/camcalib/cam4_calib.yaml', 'r'))

    cap0 = cv2.VideoCapture(0)
    cap2 = cv2.VideoCapture(2)
    cap4 = cv2.VideoCapture(4)

    camcaps = [cap0, cap2, cap4]
    cammtxs = [mtx0, mtx2, mtx4]
    camdists = [dist0, dist2, dist4]
    camrelhomos = [homo_rb20, homo_rb40]
    aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_250)
    arucomarkersize = 100
    nframe = 2
    denoise = True
    objnp = {}

    wh0 = [cap0.get(cv2.CAP_PROP_FRAME_WIDTH), cap0.get(cv2.CAP_PROP_FRAME_HEIGHT)]
    wh2 = [cap2.get(cv2.CAP_PROP_FRAME_WIDTH), cap2.get(cv2.CAP_PROP_FRAME_HEIGHT)]
    wh4 = [cap4.get(cv2.CAP_PROP_FRAME_WIDTH), cap4.get(cv2.CAP_PROP_FRAME_HEIGHT)]

    camnplist = [cam0np, cam2np, cam4np]
    caminfolist = [[wh0, Mat4.identMat(), mtx0], [wh2, pandamat4homo_r2, mtx2], [wh4, pandamat4homo_r4, mtx4]]
    ur3dt = UR3DDTCtl(camnplist, caminfolist)
    gui = HLabGUI(ur3dt)

    def updateview(framenplist, boxcmlist, fixedboxcmlist, fixedframenplist, stackid, fixedstackid, gui, task):
        if len(framenplist[0]) > 0:
            for axisnp in framenplist[0]:
                axisnp.removeNode()
            framenplist[0] = []
        if len(boxcmlist[0]) > 0:
            for boxcm in boxcmlist[0]:
                boxcm.removeNode()
            boxcmlist[0] = []
        stackid[0] = []
        frameavglist = af.trackobject_multicamfusion(camcaps, cammtxs, camdists, camrelhomos, aruco_dict, arucomarkersize, nframe, denoise, bandwidth=arucomarkersize*.10)

        imglist = getimglist(camcaps, cammtxs, camdists, camrelhomos,frameavglist)
        for i, img in enumerate(imglist):
            buf = img[:, :, 0].T.tostring()
            gui.camshowlist[i][0].setRamImage(buf)
            gui.camshowlist[i][1].setTexture(gui.camshowlist[i][0])

        for id in frameavglist:
            if id not in fixedstackid[0]:
                if id not in stackid[0]:
                    stackid[0].append(id)
                posvecavg = frameavglist[id][0]
                rotmatavg = frameavglist[id][1]
                framenp = base.pggen.genAxis(spos=base.pg.npToV3(posvecavg), pandamat3=base.pg.npToMat3(rotmatavg))
                framenp.reparentTo(base.render)
                framenplist[0].append(framenp)

                boxtransform = np.eye(4)
                boxtransform[:3, :3] = rotmatavg
                boxcenter = posvecavg - 45 * boxtransform[:3, 2]
                boxprim = trimesh.primitives.Box(box_extents=np.array([130, 120, 90]), box_center=boxcenter, box_transform=boxtransform)
                boxcm = cm.CollisionModel(objinit = boxprim)
                boxcm.setColor(.5,0,0,1)
                boxcm.reparentTo(base.render)
                boxcmlist[0].append(boxcm)

        for fixedframenp in fixedframenplist[0]:
            fixedframenp.reparentTo(base.render)
        for fixedboxcm in fixedboxcmlist[0]:
            fixedboxcm.setColor(.5,1,0,1)
            fixedboxcm.reparentTo(base.render)

        return task.again
    taskMgr.doMethodLater(0.01, updateview, "updateview", extraArgs=[ur3dt.framenplist, ur3dt.boxcmlist, ur3dt.fixedboxcmlist, ur3dt.fixedframenplist, ur3dt.stackid, ur3dt.fixedstackid, gui], appendTask=True)

    base.run()