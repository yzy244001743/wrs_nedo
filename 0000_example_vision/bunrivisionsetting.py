import environment.collisionmodel as cm
import trimesh.primitives as tp
import utiltools.robotmath as rm
import numpy as np
import pandaplotutils.pandactrl as pandactrl


class Env(object):

    def __init__(self, betransparent=False):
        """

        :param base:
        author: weiwei
        date: 20190429
        """

        self.__battached = False

        tablecorner = np.array([200,-250,0])
        frameoffset = np.array([-65,10,0])

        # columns
        box_extents = np.array([30,30,850])
        box_center = tablecorner+frameoffset+np.array([-15,15,425])
        boxprim = tp.Box(box_extents=box_extents, box_center=box_center)
        self.__columnfrontleft = cm.CollisionModel(objinit = boxprim)
        self.__columnfrontleft.setColor(.5,.5,.5,1)

        box_center = tablecorner+frameoffset+np.array([-15,15,425])+np.array([-630,0,0])
        boxprim = tp.Box(box_extents=box_extents, box_center=box_center)
        self.__columnbackleft = cm.CollisionModel(objinit = boxprim)
        self.__columnbackleft.setColor(.5,.5,.5,1)

        box_center = tablecorner+frameoffset+np.array([-15,15,425])+np.array([-630,1270,0])
        boxprim = tp.Box(box_extents=box_extents, box_center=box_center)
        self.__columnbackright = cm.CollisionModel(objinit = boxprim)
        self.__columnbackright.setColor(.5,.5,.5,1)

        box_center = tablecorner+frameoffset+np.array([-15,15,425])+np.array([0,1270,0])
        boxprim = tp.Box(box_extents=box_extents, box_center=box_center)
        self.__columnfrontright = cm.CollisionModel(objinit = boxprim)
        self.__columnfrontright.setColor(.5,.5,.5,1)

        # rows
        box_extents = np.array([600,30,30])
        box_center = tablecorner+frameoffset+np.array([-330,15,15])
        boxprim = tp.Box(box_extents=box_extents, box_center=box_center)
        self.__rowbottomleft = cm.CollisionModel(objinit = boxprim)
        self.__rowbottomleft.setColor(.5,.5,.5,1)

        box_extents = np.array([600,30,30])
        box_center = tablecorner+frameoffset+np.array([-330,15,15])+np.array([0,1270,0])
        boxprim = tp.Box(box_extents=box_extents, box_center=box_center)
        self.__rowbottomright = cm.CollisionModel(objinit = boxprim)
        self.__rowbottomright.setColor(.5,.5,.5,1)

        box_extents = np.array([600,30,30])
        box_center = tablecorner+frameoffset+np.array([-330,15,15])+np.array([0,0,850])
        boxprim = tp.Box(box_extents=box_extents, box_center=box_center)
        self.__rowtopleft = cm.CollisionModel(objinit = boxprim)
        self.__rowtopleft.setColor(.5,.5,.5,1)

        box_extents = np.array([600,30,30])
        box_center = tablecorner+frameoffset+np.array([-330,15,15])+np.array([0,1270,850])
        boxprim = tp.Box(box_extents=box_extents, box_center=box_center)
        self.__rowtopright = cm.CollisionModel(objinit = boxprim)
        self.__rowtopright.setColor(.5,.5,.5,1)

        box_extents = np.array([30,1300,30])
        box_center = tablecorner+frameoffset+np.array([-15,650,865])
        boxprim = tp.Box(box_extents=box_extents, box_center=box_center)
        self.__rowtopfront = cm.CollisionModel(objinit = boxprim)
        self.__rowtopfront.setColor(.5,.5,.5,1)

        box_extents = np.array([30,1300,30])
        box_center = tablecorner+frameoffset+np.array([-15,650,865])+np.array([-630,0,0])
        boxprim = tp.Box(box_extents=box_extents, box_center=box_center)
        self.__rowtopback = cm.CollisionModel(objinit = boxprim)
        self.__rowtopback.setColor(.5,.5,.5,1)

        box_extents = np.array([30,1300,30])
        box_center = tablecorner+frameoffset+np.array([-15,650,15])+np.array([-630,0,0])
        boxprim = tp.Box(box_extents=box_extents, box_center=box_center)
        self.__rowbottomback = cm.CollisionModel(objinit = boxprim)
        self.__rowbottomback.setColor(.5,.5,.5,1)

        box_extents = np.array([30,1300,30])
        box_center = tablecorner+frameoffset+np.array([-15,650,640])+np.array([-645,0,0])
        boxprim = tp.Box(box_extents=box_extents, box_center=box_center)
        self.__rowback1 = cm.CollisionModel(objinit = boxprim)
        self.__rowback1.setColor(.5,.5,.5,1)

        box_extents = np.array([30,1300,30])
        box_center = tablecorner+frameoffset+np.array([-15,650,425])+np.array([-645,0,0])
        boxprim = tp.Box(box_extents=box_extents, box_center=box_center)
        self.__rowback2 = cm.CollisionModel(objinit = boxprim)
        self.__rowback2.setColor(.5,.5,.5,1)

        box_extents = np.array([30,1300,30])
        box_center = tablecorner+frameoffset+np.array([-15,650,895])+np.array([-235,0,0])
        boxprim = tp.Box(box_extents=box_extents, box_center=box_center)
        self.__rowtop1 = cm.CollisionModel(objinit = boxprim)
        self.__rowtop1.setColor(.5,.5,.5,1)

        box_extents = np.array([30,500,30])
        boxprim = tp.Box(box_extents=box_extents)
        self.__rowtoptiltleft = cm.CollisionModel(objinit = boxprim)
        rownp4 = np.eye(4)
        rownp4[:3, :3] = rm.rodrigues([1,0,0],45)
        rownp4[:3, 3] = tablecorner+frameoffset+np.array([-15,80,825])+np.array([-265,0,0])
        self.__rowtoptiltleft.setMat(base.pg.np4ToMat4(rownp4))
        self.__rowtoptiltleft.setColor(.5,.5,.5,1)

        box_extents = np.array([30,500,30])
        boxprim = tp.Box(box_extents=box_extents)
        self.__rowtoptiltright = cm.CollisionModel(objinit = boxprim)
        rownp4 = np.eye(4)
        rownp4[:3, :3] = rm.rodrigues([1,0,0],-45)
        rownp4[:3, 3] = tablecorner+frameoffset+np.array([-15,454,825])+np.array([-265,0,0])
        self.__rowtoptiltright.setMat(base.pg.np4ToMat4(rownp4))
        self.__rowtoptiltright.setColor(.5,.5,.5,1)

        # table
        box_extents = np.array([800,1800,700])
        box_center = tablecorner+np.array([-400,900,-350])
        boxprim = tp.Box(box_extents=box_extents, box_center=box_center)
        self.__table = cm.CollisionModel(objinit = boxprim)
        self.__table.setColor(.43,.43,.4,1)

    def reparentTo(self, nodepath):
        if not self.__battached:
            self.__columnfrontleft.reparentTo(nodepath)
            self.__columnfrontright.reparentTo(nodepath)
            self.__columnbackleft.reparentTo(nodepath)
            self.__columnbackright.reparentTo(nodepath)
            self.__rowbottomleft.reparentTo(nodepath)
            self.__rowbottomright.reparentTo(nodepath)
            self.__rowtopleft.reparentTo(nodepath)
            self.__rowtopright.reparentTo(nodepath)
            self.__rowtopfront.reparentTo(nodepath)
            self.__rowtopback.reparentTo(nodepath)
            self.__rowbottomback.reparentTo(nodepath)
            self.__rowback1.reparentTo(nodepath)
            self.__rowback2.reparentTo(nodepath)
            self.__rowtop1.reparentTo(nodepath)
            self.__rowtoptiltleft.reparentTo(nodepath)
            self.__rowtoptiltright.reparentTo(nodepath)
            self.__table.reparentTo(nodepath)
            self.__battached = True

    def getstationaryobslist(self):
        """
        generate the collision model for stationary obstacles

        :return:

        author: weiwei
        date: 20180811
        """

        stationaryobslist = [self.__columnfrontleft, self.__columnfrontright, self.__columnbackleft, self.__columnbackright,
                             self.__rowbottomleft, self.__rowbottomright, self.__rowtopleft, self.__rowtopright,
                             self.__rowtopfront, self.__rowtopback, self.__rowbottomback,
                             self.__rowback1, self.__rowback2, self.__rowtop1, self.__rowtoptiltleft,
                             self.__rowtoptiltright]
        return stationaryobslist



if __name__ == '__main__':
    import utiltools.robotmath as rm
    import numpy as np
    from panda3d.core import *

    base = pandactrl.World(camp=[2700, 300, 2700], lookatp=[0, 0, 1000])
    env = Env()

    env.reparentTo(base.render)

    obscmlist = env.getstationaryobslist()
    for obscm in obscmlist:
        obscm.showcn()

    base.run()