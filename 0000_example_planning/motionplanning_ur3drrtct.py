"""
This file shows how to perform motion planning.
You need to declare three objects to initialize a motion planner:
1. a collisioncheckerball object
2. a ctcallback object
3. a motion planner (could be rrt, ddrrt, rrtconnect(recommended), ddrrtconnect, etc.

You may also declare a smoother object to smooth the path using random cut afterwards.

author: weiwei, toyonaka
date: 20190312
"""

from motionplanning import smoother as sm
from motionplanning import ctcallback as ctcb
from motionplanning import collisioncheckerball as cdck
from motionplanning.rrt import rrtconnect as rrtc
from robotsim.ur3dual import ur3dual
from robotsim.ur3dual import ur3dualmesh
from robotsim.ur3dual import ur3dualball
from pandaplotutils import pandactrl
import bunrisettingfree
import numpy as np
import copy
import utiltools.robotmath as rm
import manipulation.grip.robotiq85.robotiq85 as rtq85

# define your own ctchecker
# interface it like catchecker(robot, jnts, obstaclecmlist, objcmlist)
def ctchecker(robot, armname, jnts, obstaclecmlist, objcmlist, relmatlist):
    """
    a ctchecker must follow the same interface!

    :param robot:
    :param armname: the armname
    :param jnts: the current pose of a robot
    :param obstaclecmlist: a list of obstacles defined as instances of environment.CollisionModels
    :param objcmlist: a list of objects defined as instances of environment.CollisionModels. They are held by the target arm.
    :param relmatlist: the relative pose of the obj to the robot
    :return:
    """

    initjnts = robot.initrgtjnts
    if armname == 'lft':
        initjnts = robot.initlftjnts
    robot.movearmfk(jnts, armname)
    eepos, eerot = robot.getee()
    robot.movearmfk(initjnts, armname)
    axz = eerot[:,2]
    if rm.degree_betweenvector(axz, np.array([0,0,-1])) < 30:
        print("The angle between eez and -worldz is smaller than 10!")
        return True
    else:
        return False

base = pandactrl.World(camp=[2700, 300, 2700], lookatp=[0, 0, 1000])

objname = "tool_motordriver.stl"
env = bunrisettingfree.Env()
env.reparentTo(base.render)
obscmlist = env.getstationaryobslist()
# for obscm in obscmlist:
#     obscm.showcn()

# load obj collision model using env
objcm = env.loadobj("bunnysim.stl")

# another example -- load obj collision model independently
# this_dir, this_filename = os.path.split(__file__)
# objname = "tool_motordriver.stl"
# objpath = os.path.join(this_dir, "objects", objname)
# objcm = cm.CollisionModel(objpath)

hndfa = rtq85.Robotiq85Factory()
rgthnd = hndfa.genHand()
lfthnd = hndfa.genHand()
robot = ur3dual.Ur3DualRobot(rgthnd, lfthnd)
robot.opengripper(armname='rgt')
robot.closegripper(armname='lft')
robotball = ur3dualball.Ur3DualBall()
robotmesh = ur3dualmesh.Ur3DualMesh()
# robotnp = robotmesh.genmnp(robot)
# robotnp.reparentTo(base.render)
armname = 'rgt'
cdchecker = cdck.CollisionCheckerBall(robotball)
ctcallback = ctcb.CtCallback(base, robot, cdchecker, ctchecker, armname=armname)
smoother = sm.Smoother()

robot.goinitpose()
# robotnp = robotmesh.genmnp(robot, jawwidthrgt = 85.0, jawwidthlft=0.0)
# robotnp.reparentTo(base.render)
# base.run()

starttreesamplerate = 25
endtreesamplerate = 30
objstpos = np.array([354.5617667638,-256.0889005661,1090.5])
objstrot = np.array([[1.0,0.0,0.0],
                    [0.0,1.0,0.0],
                    [0.0,0.0,1.0]]).T
objgpos = np.array([304.5617667638,-277.1026725769,1101.0])
objgrot = np.array([[1.0,0.0,0.0],
                   [0.0,6.12323426293e-17,-1.0],
                   [0.0,1.0,6.12323426293e-17]]).T
objcmcopys = copy.deepcopy(objcm)
objcmcopys.setColor(0.0,0.0,1.0,.3)
objcmcopys.setMat(base.pg.npToMat4(npmat3=objstrot,npvec3=objstpos))
objcmcopys.reparentTo(base.render)

startpos = np.array([354.5617667638,-256.0889005661,1060.5])
startrot = np.array([[1,0,0],
                     [0,-1,0],
                     [0,0,-1]]).T
goalpos3 = np.array([354.5617667638,56.0889005661,1150.5])
goalrot3 = np.array([[1,0,0],
                    [0,-0.92388,-0.382683],
                    [0,0.382683,-0.92388]]).T
goalrot3 = np.dot(rm.rodrigues([1,0,0],20),goalrot3)
goalpos3 = goalpos3 - 70.0*goalrot3[:,2]
goalpos4 = np.array([154.5617667638,-500,1000.5])
goalrot4 = np.dot(rm.rodrigues([1,0,0],-80),goalrot3)
goalpos4 = goalpos4 - 150.0*goalrot4[:,2]
# goalpos2 = goalpos2 - 150.0*goalrot2[:,2]
start = robot.numik(startpos, startrot, armname)
goal3 = robot.numikmsc(goalpos3, goalrot3, msc=start, armname=armname)
print(goal3)
goal4 = robot.numikmsc(goalpos4, goalrot4, msc=start, armname=armname)
# robot.movearmfk(goal3, armname)
# robotmesh.genmnp(robot).reparentTo(base.render)
# robotball.showcn(robotball.genfullbcndict(robot))
# base.run()
print(goal4)
planner = rrtc.RRTConnect(start=goal3, goal=goal4, ctcallback=ctcallback,
                              starttreesamplerate=starttreesamplerate,
                              endtreesamplerate=endtreesamplerate, expanddis=5,
                              maxiter=2000, maxtime=100.0)
robot.movearmfk(goal3, armname)
robotnp = robotmesh.genmnp(robot)
robotnp.reparentTo(base.render)
robot.movearmfk(goal4, armname)
robotnp = robotmesh.genmnp(robot)
robotnp.reparentTo(base.render)
robot.goinitpose()
[path, sampledpoints] = planner.planning(obscmlist+[objcmcopys])
path = smoother.pathsmoothing(path, planner)
print(path)
# for pose in path:
#     robot.movearmfk(pose, armname)
#     robotstick = robotmesh.gensnp(robot = robot)
#     robotstick.reparentTo(base.render)
# base.pggen.plotAxis(base.render, spos=goalpos, pandamat3 = base.pg.npToMat3(goalrot))



def update(rbtmnp, motioncounter, robot, path, armname, robotmesh, task):
    if motioncounter[0] < len(path):
        if rbtmnp[0] is not None:
            rbtmnp[0].detachNode()
            # rbtmnp[1].detachNode()
        pose = path[motioncounter[0]]
        robot.movearmfk(pose, armname)
        rbtmnp[0] = robotmesh.genmnp(robot)
        # bcndict = robotball.genfullactivebcndict(robot)
        # rbtmnp[1] = robotball.showcn(bcndict)
        rbtmnp[0].reparentTo(base.render)
        motioncounter[0] += 1
    else:
        motioncounter[0] = 0
    return task.again

rbtmnp = [None, None]
motioncounter = [0]
taskMgr.doMethodLater(0.05, update, "update",
                      extraArgs=[rbtmnp, motioncounter, robot, path, armname, robotmesh],
                      appendTask=True)

base.run()