import pblgrpc.camclient as cc
from panda3d.core import Texture
from pandaplotutils import pandactrl as pandactrl
from direct.gui.DirectGui import OnscreenImage
import cv2

if __name__=="__main__":

    # base = pandactrl.World(camp = [5000,-3000,3000], lookatp = [0,0,700])
    # # Define a hand camera capture (hcc) object and get an image from right hand camera 0 (rc0).
    # # The function runs grpc underneath
    # hcc = cc.Cam(host="localhost:18300")
    # imgx = hcc.getimgbytes()
    # imgh, imgw, nch = imgx.shape
    # txtre = Texture()
    # txtre.setup2dTexture(int(imgw), int(imgh), Texture.T_unsigned_byte, Texture.F_rgb)
    # txtre.setRamImage(cv2.flip(imgx,0))
    # OnscreenImage(image=txtre, pos=(0, 0, 0), scale=(imgw/imgh, 1, 1))
    #
    # def updateview(hcc, txtre, task):
    #     imgx = hcc.getimgbytes()
    #     imgh, imgw, nch = imgx.shape
    #     txtre.setup2dTexture(int(imgw), int(imgh), Texture.T_unsigned_byte, Texture.F_rgb)
    #     txtre.setRamImage(cv2.flip(imgx,0))
    #     return task.again
    #
    # taskMgr.doMethodLater(0.01, updateview, "updateview", extraArgs=[hcc, txtre], appendTask=True)
    # base.run()

    hcc = cc.Cam(host="localhost:18300")
    while True:
        imgx = hcc.getimgbytes()
        cv2.imshow("test", imgx)
        cv2.waitKey(1)