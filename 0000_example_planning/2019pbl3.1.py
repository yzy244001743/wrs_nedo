from pandaplotutils import pandactrl as pandactrl
import manipulation.grip.robotiq85.robotiq85 as rtq85
import robotsim.ur3dual.ur3dual as robot
import robotsim.ur3dual.ur3dualmesh as robotmesh
import utiltools.robotmath as rm
import numpy as np
from robotsim.ur3dual import ur3dualball
from motionplanning import collisioncheckerball as cdck
import bunrisettingfree as bsf
from motionplanning import ctcallback as ctcb
from motionplanning.rrt import rrtconnect as rrtc
from panda3d.core import Vec4
from motionplanning import smoother as sm

if __name__=="__main__":

    base = pandactrl.World(camp = [3000,-3000,7000], lookatp = [0,0,700])

    hndfa = rtq85.Robotiq85Factory()
    rgthnd = hndfa.genHand()
    lfthnd = hndfa.genHand()
    rbt = robot.Ur3DualRobot(rgthnd, lfthnd)
    rbt.goinitpose()
    rbtball = ur3dualball.Ur3DualBall()
    rbtmg = robotmesh.Ur3DualMesh()

    env = bsf.Env(boundingradius=5)
    env.reparentTo(base.render)
    obscmlist = env.getstationaryobslist()
    for obscm in obscmlist:
        obscm.showcn()

    armname="rgt"
    start = rbt.getarmjnts(armname=armname)
    start[0] = start[0]+90.0
    start[3] = start[3]-90.0
    rbt.movearmfk(start, armname=armname)
    rbtmg.genmnp(rbt, togglejntscoord=False, rgbargt=[1, 0.0, 0.0, .5]).reparentTo(base.render)
    # rbtball.showfullcn(rbt)

    # eepos = [300.0, 0.0, 1400.0]
    # eerot = np.dot(rm.rodrigues([1,0,0], -180), np.eye(3))
    # goal = rbt.numik(eepos, eerot, armname="rgt")
    # rbt.movearmfk(goal, armname=armname)
    # rbtmg.genmnp(rbt, togglejntscoord=False, rgbargt=[0.0, 0.0, 1.0, .5]).reparentTo(base.render)
    # rbtball.showfullcn(rbt)
    #
    # eepos = [-300.0, 0.0, 1400.0]
    # eerot = np.dot(rm.rodrigues([1,0,0], -180), np.eye(3))
    # goal = rbt.numik(eepos, eerot, armname="rgt")
    # rbt.movearmfk(goal, armname=armname)
    # rbtmg.genmnp(rbt, togglejntscoord=False, rgbargt=[1.0, 1.0, 0.0, .5]).reparentTo(base.render)
    # cdchecker = cdck.CollisionCheckerBall(rbtball)
    # print(cdchecker.isRobotObstacleCollided(rbt, obscmlist))
    # rbtball.showfullcn(rbt)
    #
    # eepos = [200.0, -300.0, 1400.0]
    # eerot = np.dot(rm.rodrigues([1,0,0], -180), np.eye(3))
    # goal = rbt.numik(eepos, eerot, armname="rgt")
    # rbt.movearmfk(goal, armname=armname)
    # rbtmg.genmnp(rbt, togglejntscoord=False, rgbargt=[0.0, 1.0, 1.0, .5]).reparentTo(base.render)
    # print(cdchecker.isRobotObstacleCollided(rbt, obscmlist))
    # rbtball.showfullcn(rbt)


    # ctcallback = ctcb.CtCallback(base, rbt, cdchecker=cdchecker, ctchecker=None, armname=armname)
    # print(start, goal)
    # planner = rrtc.RRTConnect(start=start, goal=goal, ctcallback=ctcallback,
    #                             starttreesamplerate=30,
    #                             endtreesamplerate=30, expanddis=5,
    #                             maxiter=2000, maxtime=100.0)
    # path, samples = planner.planning()
    # for i, armjnts in enumerate(path):
    #     rbt.movearmfk(armjnts, armname)
    #     # rbtmg.genmnp(rbt, togglejntscoord=False, rgbargt=[1.0-i/len(path), 0.0, i/len(path), .2]).reparentTo(base.render)
    #     # rbtmg.genesnp(rbt, rgtrgba=[0.5,0.5,0,1]).reparentTo(base.render)
    #
    # smoother = sm.Smoother()
    # path = smoother.pathsmoothing(path, planner)
    # for i, armjnts in enumerate(path):
    #     rbt.movearmfk(armjnts, armname)
    #     # rbtmg.genmnp(rbt, togglejntscoord=False, rgbargt=[1.0-i/len(path), 0.0, i/len(path), .2]).reparentTo(base.render)
    #     rbtmg.genesnp(rbt, rgtrgba=[0,0.5,0,1]).reparentTo(base.render)
    #
    #
    # def update(rbtmnp, motioncounter, rbt, path, armname, rbtmg, task):
    #     if motioncounter[0] < len(path):
    #         if rbtmnp[0] is not None:
    #             rbtmnp[0].detachNode()
    #         pose = path[motioncounter[0]]
    #         rbt.movearmfk(pose, armname)
    #         rbtmnp[0] = rbtmg.genmnp(rbt, 0, 0)
    #         rbtmnp[0].reparentTo(base.render)
    #         motioncounter[0] += 1
    #     else:
    #         motioncounter[0] = 0
    #     return task.again
    #
    #
    # rbtmnp = [None]
    # motioncounter = [0]
    # taskMgr.doMethodLater(0.05, update, "update",
    #                       extraArgs=[rbtmnp, motioncounter, rbt, path, armname, rbtmg],
    #                       appendTask=True)

    base.run()