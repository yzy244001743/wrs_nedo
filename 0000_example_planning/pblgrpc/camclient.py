import grpc
import yaml
import pblgrpc.cam_pb2 as cammsg
import pblgrpc.cam_pb2_grpc as camrpc
import numpy as np

import cv2

class Cam(object):

    def __init__(self, host = "localhost:18300"):
        self.__oldyaml = True
        if int(yaml.__version__[0]) >= 5:
            self.__oldyaml = False
        channel = grpc.insecure_channel(host)
        self.stub = camrpc.CamStub(channel)

    def getimgstr(self):
        if self.__oldyaml:
            return yaml.load(self.stub.getimgstr(cammsg.Empty()).data)
        else:
            return yaml.load(self.stub.getimgstr(cammsg.Empty()).data, Loader=yaml.UnsafeLoader)

    def getimgbytes(self):
        message_image = self.stub.getimgbytes(cammsg.Empty())
        imgw = message_image.width
        imgh = message_image.height
        imgnch = message_image.channel
        imgbytes = message_image.image
        re_img = np.frombuffer(imgbytes, dtype=np.uint8)
        # Convert back the data to original image shape
        re_img = np.reshape(re_img, (imgh, imgw, imgnch))
        return re_img

if __name__=="__main__":
    import time

    hcc = Cam(host = "localhost:18300")
    # tic = time.time()
    # imgx = hcc.getimgbytes()
    # toc = time.time()
    # td = toc-tic
    # tic = time.time()
    # imgxs = hcc.getimgstr()
    # toc = time.time()
    # td2 = toc-tic
    # print(td, td2)
    while True:
        imgx = hcc.getimgstr()
        cv2.imshow("name", imgx)
        cv2.waitKey(1)