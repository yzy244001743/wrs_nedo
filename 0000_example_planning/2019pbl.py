import pandaplotutils.pandactrl as pc
import utiltools.robotmath as rm
import numpy as np
import environment.collisionmodel as cm
import trimesh as tm
import math
from panda3d.core import Mat4

if __name__=="__main__":
    base = pc.World(camp=[1200, 1200, 1200], lookatp=[0, 0, 0])

    pdidmat4 = Mat4.identMat()
    base.pggen.plotAxis(base.render, pandamat3=pdidmat4.getUpper3())
    base.run()

    # sphere = cm.CollisionModel(tm.primitives.Sphere(subdivisions=5, sphere_radius=340), betransparency=True)
    # sphere.setColor(.7,.7,.7,1)
    # sphere.reparentTo(base.render)
    base.pggen.plotAxis(base.render)

    ra = np.array((150, 200, 50))
    rotmat = rm.euler_matrix(45, -30, 60, "sxyz")
    # # print(rm.euler_from_matrix(rotmat, "rzxz"))
    # rotmat = rm.euler_matrix(45, 0, 0, "sxyz")
    # base.pggen.plotAxis(base.render, pandamat3 = base.pg.npToMat3(rotmat), thickness = 9.7, alpha=.1, rgba=[])
    # # base.pggen.plotCircArrow(base.render, axis=[1,0,0], torqueportion=0.9, center = [150,0,0], radius = 70, thickness=7, rgba=[0.5,0.5,0.5,1])
    # rotmat = rm.euler_matrix(45, -30, 0, "sxyz")
    # base.pggen.plotAxis(base.render, pandamat3 = base.pg.npToMat3(rotmat), thickness = 9.7, alpha=.3)
    # # base.pggen.plotCircArrow(base.render, axis=[0,-1,0], torqueportion=0.9, center = [0,150,0], radius = 70, thickness=7, rgba=[0.5,0.5,0.5,1])
    # rotmat = rm.euler_matrix(45, -30, 60, "sxyz")
    # base.pggen.plotAxis(base.render, pandamat3 = base.pg.npToMat3(rotmat), thickness = 9.7, alpha=.5)
    # base.pggen.plotCircArrow(base.render, axis=[0, 0, 1], torqueportion=0.9, center=[0, 0, 150], radius=70, thickness=7, rgba=[0.5, 0.5, 0.5, 1])
    # base.pggen.plotTorus(base.render, axis=[0, 0, 1], center=[0, 0, 0], radius=300+40, thickness=7, rgba=[1, .5, 0, 1], discretization=128)
    # base.pggen.plotTorus(base.render, axis=rotmat[:,2], center=[0, 0, 0], radius=300+40, thickness=7, rgba=[1, 1, 0.1, 1], discretization=128)

    v0 = rm.unitvec_safe(np.cross(np.array([0,0,1]), rotmat[:,2]))[1]
    # base.pggen.plotArrow(base.render, spos=np.array([0,0,0]), epos = v0, length=300, thickness=10, rgba=[1, .5, 0, 1])
    # base.pggen.plotArrow(base.render, spos=np.array([0,0,0]), epos = v0, length=300, thickness=10, rgba=[1, 1, 0.1, 1])
    # base.pggen.plotArrow(base.render, spos=np.array([0,0,0]), epos = v0, length=300, thickness=10, rgba=[1, .7, 0.05, 1])
    # base.pggen.plotSphere(base.render, pos= v0*340, radius=20, rgba=[0, .0, 0.0, 1])
    v0n = np.dot(rotmat, v0)
    # base.pggen.plotArrow(base.render, spos=np.array([0,0,0]), epos = v0n, length=300, thickness=10, rgba=[1, 1, 0.1, 1])
    # base.pggen.plotSphere(base.render, pos= v0n*340, radius=20, rgba=[0, .0, 0.0, 1])
    v0p = np.dot(rotmat.T, v0)
    # base.pggen.plotArrow(base.render, spos=np.array([0,0,0]), epos = v0p, length=300, thickness=10, rgba=[1, .5, 0, 1])
    # base.pggen.plotSphere(base.render, pos= v0p*340, radius=20, rgba=[0, .0, 0.0, 1])
    # base.pggen.plotDumbbell(base.render, spos = v0*340, epos = v0n*340, thickness=10, rgba=[0, 0, 0, 1])
    # base.pggen.plotDumbbell(base.render, spos = v0p*340, epos =v0*340, thickness=10, rgba=[0, 0, 0,1])
    # base.pggen.plotDumbbell(base.render, spos = v0p*340, epos =v0n*340, thickness=10, rgba=[0, 0, 0,1])
    rvec = rm.unitvec_safe(np.cross(v0n-v0, v0p-v0))[1]

    # center = np.dot(v0p * 340, rvec) * rvec
    # rotmat = np.eye(4)
    # rotmat[:3,0]=v0-v0p
    # rotmat[:3,1]=rm.unitvec_safe(np.cross(rvec, rotmat[:3,0]))[1]
    # rotmat[:3,2]=rvec
    # box = cm.CollisionModel(tm.primitives.Box(box_extents=[700,700,1], box_center = center, box_transform=rotmat))
    # box.setColor(.7,.7,.7,1)
    # box.reparentTo(base.render)
    # base.pggen.plotArrow(base.render, spos=np.array([0,0,0]), epos = rvec, length=300, thickness=10, rgba=[0, 0, 0,1])
    # center = np.dot(v0*340, rvec)*rvec
    # radius = np.linalg.norm(v0*340-center)
    # base.pggen.plotTorus(base.render, axis=rvec, center=center, radius=radius, thickness=7, rgba=[0, 0, 0, 1], discretization=128)

    newvec = np.dot(rm.rodrigues([0,0,1], 30),rvec)
    base.pggen.plotArrow(base.render, spos=np.array([0,0,0]), epos = newvec, length=700, thickness=10, rgba=[1, 1, 1,1])
    base.pggen.plotSphere(base.render, pos= newvec*740, radius=10, rgba=[0, .0, 0.0, 1])
    rotatednewvec = np.dot(rm.rodrigues(rvec, 30), newvec)
    base.pggen.plotArrow(base.render, spos=np.array([0,0,0]), epos = rotatednewvec, length=700, thickness=10, rgba=[.5, .5, .5,1])
    base.pggen.plotSphere(base.render, pos= rotatednewvec*740, radius=10, rgba=[0, .0, 0.0, 1])
    center = np.dot(newvec*740, rvec)*rvec
    radius = np.linalg.norm(newvec*740-center)
    base.pggen.plotTorus(base.render, axis=rvec, center=center, radius=radius, thickness=7, rgba=[1, 1, 1, 1], discretization=128)

    base.pggen.plotSphere(base.render, pos= center, radius=10, rgba=[0, .0, 0.0, 1])
    base.pggen.plotArrow(base.render, spos = center, epos = newvec*740, length = np.linalg.norm(newvec*740-center)-40, thickness=10, rgba=[1, 1, 0.1, 1])
    base.pggen.plotArrow(base.render, spos = center, epos = rotatednewvec*740, length = np.linalg.norm(rotatednewvec*740-center)-40, thickness=10, rgba=[1, .5, 0, 1])
    base.pggen.plotArrow(base.render, spos=np.array([0,0,0]), epos = rvec, length=np.linalg.norm(center)-40, thickness=10, rgba=[.47, 0, .47,1])


    # base.pggen.plotArrow(base.render, spos = center, epos = (newvec*340-center)*math.cos(math.radians(30))+center, length = np.linalg.norm(newvec*340-center)-40, thickness=10, rgba=[1, .5, 0, 1])

    # angle, axis = rm.quaternion_angleaxis(rm.quaternion_from_matrix(rotmat))
    # print(axis, rvec)
    # for i in range(5,int(angle),5):
    #     rotmat = rm.rodrigues(axis,i)
    #     base.pggen.plotTorus(base.render, axis=rotmat[:,2], center=[0, 0, 0], radius=300+40, thickness=7, rgba=[1, .5+.5*i/int(angle), 0+0.1*i/int(angle), 1], discretization=128)
    # base.pggen.plotTorus(base.render, axis=[0, 0, 1], center=[0, 0, 0], radius=300+40, thickness=7, rgba=[1, .5, 0, 1], discretization=128)
    # base.pggen.plotTorus(base.render, axis=rotmat[:,2], center=[0, 0, 0], radius=300+40, thickness=7, rgba=[1, 1, 0.1, 1], discretization=128)

    # center = np.dot(np.array([0,0,1])*340, rvec)*rvec
    # radius = np.linalg.norm(np.array([0,0,1])*340-center)
    # base.pggen.plotTorus(base.render, axis=rvec, center=center, radius=radius, thickness=7, rgba=[0, 0, 1, 1], discretization=128)
    # center = np.dot(np.array([0,1,0])*340, rvec)*rvec
    # radius = np.linalg.norm(np.array([0,1,0])*340-center)
    # base.pggen.plotTorus(base.render, axis=rvec, center=center, radius=radius, thickness=7, rgba=[0, 1, 0, 1], discretization=128)
    # center = np.dot(np.array([1,0,0])*340, rvec)*rvec
    # radius = np.linalg.norm(np.array([1,0,0])*340-center)
    # base.pggen.plotTorus(base.render, axis=rvec, center=center, radius=radius, thickness=7, rgba=[1, 0, 0, 1], discretization=128)

    # rotmat = rm.euler_matrix(-61.1, 0, 0, "sxyx")
    # base.pggen.plotAxis(base.render, pandamat3 = base.pg.npToMat3(rotmat), thickness = 9.7, alpha=.1)
    # # base.pggen.plotCircArrow(base.render, axis=[-1,0,0], torqueportion=0.9, center = [150,0,0], radius = 70, thickness=7, rgba=[0.5,0.5,0.5,1])
    # rotmat = rm.euler_matrix(-61.1, 64.34, 0, "sxyx")
    # base.pggen.plotAxis(base.render, pandamat3 = base.pg.npToMat3(rotmat), thickness = 9.7, alpha=.3)
    # # base.pggen.plotCircArrow(base.render, axis=[0,1,0], torqueportion=0.9, center = [0,150,0], radius = 70, thickness=7, rgba=[0.5,0.5,0.5,1])
    # rotmat = rm.euler_matrix(-61.1, 64.34, 123.69, "sxyx")
    # base.pggen.plotAxis(base.render, pandamat3 = base.pg.npToMat3(rotmat), thickness = 9.7, alpha=.5)
    # base.pggen.plotCircArrow(base.render, axis=[1,0,0], torqueportion=0.9, center = [150,0,0], radius = 70, thickness=7, rgba=[0.5,0.5,0.5,1])

    # rotmat = rm.euler_matrix(33.43, 52.24, 39.23, "rzxz")
    # rotmat = rm.euler_matrix(33.43, 0, 0, "rzxz")
    # base.pggen.plotAxis(base.render, pandamat3 = base.pg.npToMat3(rotmat), thickness = 9.7, alpha=.1)
    # # base.pggen.plotCircArrow(base.render, axis=[0,0,1], torqueportion=0.9, center = [0,0,150], radius = 70, thickness=7, rgba=[0.5,0.5,0.5,1])
    # rotmat2 = rm.euler_matrix(33.43, 52.24, 0, "rzxz")
    # base.pggen.plotAxis(base.render, pandamat3 = base.pg.npToMat3(rotmat2), thickness = 9.7, alpha=.3)
    # # base.pggen.plotCircArrow(base.render, axis=rotmat[:,0], torqueportion=0.9, center = 150*rotmat[:,0], radius = 70, thickness=7, rgba=[0.5,0.5,0.5,1])
    # rotmat3 = rm.euler_matrix(33.43, 52.24, 39.23, "rzxz")
    # base.pggen.plotAxis(base.render, pandamat3 = base.pg.npToMat3(rotmat3), thickness = 10, alpha=.5)
    # base.pggen.plotCircArrow(base.render, axis=rotmat2[:,2], torqueportion=0.9, center = 150*rotmat2[:,2], radius = 70, thickness=7, rgba=[0.5,0.5,0.5,1])

    # ro = np.dot(rotmat, ra)
    # base.pggen.plotSphere(base.render, pos=ro, radius=10, rgba=[0,0,0,1])

    # # uv
    # epos = ra[0]*rotmat[:,0]+ra[1]*rotmat[:,1]
    # base.pggen.plotDumbbell(base.render, spos = ro, epos=epos, rgba=[0,0,0,1])
    # base.pggen.plotDumbbell(base.render, spos = epos, epos=np.dot(epos, rotmat[:,0])*rotmat[:,0], rgba=[0,0,0,1])
    # base.pggen.plotDumbbell(base.render, spos = epos, epos=np.dot(epos, rotmat[:,1])*rotmat[:,1], rgba=[0,0,0,1])
    # # uw
    # epos = ra[0]*rotmat[:,0]+ra[2]*rotmat[:,2]
    # base.pggen.plotDumbbell(base.render, spos = ro, epos=epos, rgba=[0,0,0,1])
    # base.pggen.plotDumbbell(base.render, spos = epos, epos=np.dot(epos, rotmat[:,0])*rotmat[:,0], rgba=[0,0,0,1])
    # base.pggen.plotDumbbell(base.render, spos = epos, epos=np.dot(epos, rotmat[:,2])*rotmat[:,2], rgba=[0,0,0,1])
    # # vw
    # epos = ra[1]*rotmat[:,1]+ra[2]*rotmat[:,2]
    # base.pggen.plotDumbbell(base.render, spos = ro, epos=epos, rgba=[0,0,0,1])
    # base.pggen.plotDumbbell(base.render, spos = epos, epos=np.dot(epos, rotmat[:,1])*rotmat[:,1], rgba=[0,0,0,1])
    # base.pggen.plotDumbbell(base.render, spos = epos, epos=np.dot(epos, rotmat[:,2])*rotmat[:,2], rgba=[0,0,0,1])

    # # xy
    # omat = np.eye(3)
    # epos = ro[0]*omat[:,0]+ro[1]*omat[:,1]
    # base.pggen.plotDumbbell(base.render, spos = ro, epos=epos, rgba=[0,0,0,1])
    # base.pggen.plotDumbbell(base.render, spos = epos, epos=np.dot(epos, omat[:,0])*omat[:,0], rgba=[0,0,0,1])
    # base.pggen.plotDumbbell(base.render, spos = epos, epos=np.dot(epos, omat[:,1])*omat[:,1], rgba=[0,0,0,1])
    # # xz
    # epos = ro[0]*omat[:,0]+ro[2]*omat[:,2]
    # base.pggen.plotDumbbell(base.render, spos = ro, epos=epos, rgba=[0,0,0,1])
    # base.pggen.plotDumbbell(base.render, spos = epos, epos=np.dot(epos, omat[:,0])*omat[:,0], rgba=[0,0,0,1])
    # base.pggen.plotDumbbell(base.render, spos = epos, epos=np.dot(epos, omat[:,2])*omat[:,2], rgba=[0,0,0,1])
    # # yz
    # epos = ro[1]*omat[:,1]+ro[2]*omat[:,2]
    # base.pggen.plotDumbbell(base.render, spos = ro, epos=epos, rgba=[0,0,0,1])
    # base.pggen.plotDumbbell(base.render, spos = epos, epos=np.dot(epos, omat[:,1])*omat[:,1], rgba=[0,0,0,1])
    # base.pggen.plotDumbbell(base.render, spos = epos, epos=np.dot(epos, omat[:,2])*omat[:,2], rgba=[0,0,0,1])

    base.run()