from pandaplotutils import pandactrl as pandactrl
import manipulation.grip.robotiq85.robotiq85 as rtq85
import robotsim.ur3dual.ur3dual as robot
import robotsim.ur3dual.ur3dualmesh as robotmesh
import utiltools.robotmath as rm
import numpy as np
import environment.collisionmodel as cm

if __name__=="__main__":

    base = pandactrl.World(camp = [300,300,500], lookatp = [0,0,60])

    base.pggen.plotAxis(base.render, length=120, thickness=3)

    obj = cm.CollisionModel(objinit="./objects/domino.stl")
    obj.setColor(.8, .6, .3, .5)
    obj.reparentTo(base.render)

    hndfa = rtq85.Robotiq85Factory()
    pregrasps = []
    rtq85 = hndfa.genHand()
    base.pggen.plotAxis(rtq85.handnp, length=15, thickness=2)
    pregrasps.append(rtq85.approachAt(0,0,90,0,1,0,-.707,0,-.707,jawwidth=60))
    rtq85.reparentTo(base.render)
    rtq85 = hndfa.genHand()
    base.pggen.plotAxis(rtq85.handnp, length=15, thickness=2)
    pregrasps.append(rtq85.approachAt(0,0,90,0,1,0,.707,0,-.707,jawwidth=60))
    rtq85.reparentTo(base.render)
    rtq85 = hndfa.genHand()
    base.pggen.plotAxis(rtq85.handnp, length=15, thickness=2)
    pregrasps.append(rtq85.approachAt(0,0,30,0,1,0,-.707,0,.707,jawwidth=60))
    rtq85.reparentTo(base.render)
    rtq85 = hndfa.genHand()
    base.pggen.plotAxis(rtq85.handnp, length=15, thickness=2)
    pregrasps.append(rtq85.approachAt(0,0,30,0,1,0,.707,0,.707,jawwidth=60))
    rtq85.reparentTo(base.render)

    base.run()