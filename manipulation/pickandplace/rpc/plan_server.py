import grpc
import time
import yaml
import numpy as np
from concurrent import futures
import rpc.plan_pb2 as pmsg
import rpc.plan_pb2_grpc as prpc


class PlanServer(prpc.PlanServicer):

    def initialize(self, wszero = np.array([0,0,0])):
        """

        :param wszero: workspace zero position
        :return:

        author: weiwei
        date: 20190429
        """

        self.__oldyaml = True
        if int(yaml.__version__[0]) >= 5:
            self.__oldyaml = False
        self.wszero = wszero
        self.poselist = []

    def setgoals(self, request, context):
        """
        Inherited from the auto-generated plan_pb2_grpc

        :param request:
        :param context:
        :return:

        author: weiwei
        date: 20190429
        """

        if self.__oldyaml:
            poselist = yaml.load(request.data)
        else:
            poselist = yaml.load(request.data, Loader=yaml.UnsafeLoader)

        for i, pose in enumerate(poselist):
            poselist[i][0] = pose[0]+self.wszero
        self.poselist = poselist
        print(poselist)
        return pmsg.Empty2()

def serve():
    """

    :param nodepath: where to append objects to
    :return:
    """

    _ONE_DAY_IN_SECONDS = 60 * 60 * 24

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    pserv = PlanServer()
    pserv.initialize()
    prpc.add_PlanServicer_to_server(pserv, server)
    server.add_insecure_port('[::]:18300')
    server.start()
    print("The Plan server is started!")
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)

if __name__ == '__main__':
    serve()