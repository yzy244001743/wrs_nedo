import os
import time
import numpy as np
from panda3d.core import Mat4
import pandaplotutils.pandactrl as pandactrl
import pandaplotutils.pandageom as pandageom
from manipulation.grip import freegripcontactpairs as fgcp
from database import dbcvt as dc
import environment.bulletcdhelper as bcd
import utiltools.robotmath as rm

class Freegrip(fgcp.FreegripContactpairs):
    """
    Plan free grips without using precc
    """

    def __init__(self, objpath, hand, faceangle = .9, segangle = .9, refine1min=2, refine1max=30,
                 refine2radius=5, fpairparallel=-0.7, hmax=5, objmass = 20.0, bypasssoftfgr = True, togglebcdcdebug = False, useoverlap = True):
        """

        :param objpath: path of the object
        :param hand: An object of a 2F hand class
        :param faceangle: The threhold angle for two triangles to be considered as co-planer
        :param segangle: The threhold angle for two facets to be considered as co-planer
        :param refine1min: The minimum distance between a contact point and the facet boundary
        :param refine1max: The maximum distance between a contact point and the facet boundary
        :param refine2radius: Size of the contact circle
        :param fpairparallel: The threhold dot product for two facets to be considered as parallel
        :param hmax: The maximum punction depth of a soft finger contact model
        :param objmass: Mass of the object
        :param bypasssoftfgr: Boolean value to switch on the soft finger model. True means it is switched off by default
        :param togglebcdcdebug: show bullet meshes or not
        :param useoverlap: for comparison, toggle it off if overlaped segments are needed

        author: weiwei
        date: 20161201osaka, 20190516osaka,
        """

        super().__init__(objpath, faceangle=faceangle, segangle=segangle, refine1min=refine1min, refine1max=refine1max,
                 refine2radius=refine2radius, fpairparallel=fpairparallel, hmax=hmax, objmass = objmass,
                         bypasssoftfgr = bypasssoftfgr, useoverlap=useoverlap)

        self.hand = hand
        self.handname = hand.name
        self.objname = self.objcm.name

        self.gripcontacts_planned = None
        self.griprotmats_planned = None
        self.gripjawwidth_planned = None
        self.gripcontactnormals_planned = None

        # collision detection
        print("number of vertices", len(self.objtrimesh.vertices))
        print("number of faces", len(self.objtrimesh.faces))
        self.__togglebcdcdebug = togglebcdcdebug
        self.bcdc = bcd.MCMchecker(toggledebug = self.__togglebcdcdebug)

    def planGrasps(self, discretesize=8, contactoffset = 10):
        """
        plan the grasps
        the hand poses will be divided into two categories
        one is the collision free hand poses, they will be saved in
            self.gripcontacts
            self.griprotmats
            self.gripjawwidth
            self.gripcontactnormals
        one is the hand poses that collide with the given object, they will be saved in

        :param discretesize: the number of hand orientations
        :param contactoffset: The offset from the contact point.
                It is used to move the finger fads some distance away from the contact point.
        :return:

        author: weiwei
        date: 20161212tsukuba, 20190516osaka
        """

        self.gripcontacts_planned = []
        self.griprotmats_planned = []
        self.gripjawwidth_planned = []
        self.gripcontactnormals_planned = []
        self.gripcontacts_removed = []
        self.griprotmats_removed = []
        self.gripjawwidth_removed = []
        self.gripcontactnormals_removed = []

        counter_facetpair = 0

        while counter_facetpair < len(self.facetpairs):
            for j, contactpair in enumerate(self.gripcontactpairs[counter_facetpair]):
                fingerdistance = np.linalg.norm((contactpair[0] - contactpair[1]))+contactoffset
                if fingerdistance > self.hand.jawwidthopen:
                    continue
                fingercenter = (contactpair[0] + contactpair[1])/2.0
                contactnormal0 = self.gripcontactpairnormals[counter_facetpair][j][0]
                contactnormal1 = self.gripcontactpairnormals[counter_facetpair][j][1]
                avgcontactnormal = rm.unitvec_safe(contactnormal0-contactnormal1)[1]
                for angleid in range(discretesize):
                    rotangle = 360.0 / discretesize * angleid
                    self.hand.gripAt(fingercenter[0], fingercenter[1], fingercenter[2],
                                     avgcontactnormal[0], avgcontactnormal[1], avgcontactnormal[2],
                                     rotangle, jawwidth = fingerdistance)
                    # collision detection
                    result = self.bcdc.isMeshMeshListCollided(self.objcm, self.hand.cmlist)

                    if not result:
                        # contact0 first
                        self.gripcontacts_planned.append(contactpair)
                        pdrotmat4 = Mat4(self.hand.getMat())
                        self.griprotmats_planned.append(pdrotmat4)
                        self.gripjawwidth_planned.append(fingerdistance)
                        self.gripcontactnormals_planned.append([avgcontactnormal, -avgcontactnormal])
                        # flip
                        self.gripcontacts_planned.append([contactpair[1], contactpair[0]])
                        # rotate 180 around local z
                        pdrotmat4flip = Mat4(pdrotmat4)
                        pdrotmat4flip.setRow(0, -pdrotmat4.getRow3(0))
                        pdrotmat4flip.setRow(1, -pdrotmat4.getRow3(1))
                        self.griprotmats_planned.append(pdrotmat4flip)
                        self.gripjawwidth_planned.append(fingerdistance)
                        self.gripcontactnormals_planned.append([-avgcontactnormal, avgcontactnormal])
                    # else:
                    #     # contact0 first
                    #     self.gripcontacts_removed.append(contactpair)
                    #     pdrotmat4 = self.hand.getMat()
                    #     self.griprotmats_removed.append(pdrotmat4)
                    #     self.gripjawwidth_removed.append(fingerdistance)
                    #     self.gripcontactnormals_removed.append([avgcontactnormal, -avgcontactnormal])
                    #     # flip
                    #     self.gripcontacts_removed.append([contactpair[1], contactpair[0]])
                    #     # rotate 180 around local z
                    #     pdrotmat4flip = Mat4(pdrotmat4)
                    #     pdrotmat4flip.setRow(0, -pdrotmat4.getRow3(0))
                    #     pdrotmat4flip.setRow(1, -pdrotmat4.getRow3(1))
                    #     self.griprotmats_removed.append(pdrotmat4flip)
                    #     self.gripjawwidth_removed.append(fingerdistance)
                    #     self.gripcontactnormals_removed.append([-avgcontactnormal, avgcontactnormal])

            counter_facetpair+=1

    def saveToDB(self, gdb):
        """
        save the result to mysqldatabase

        :param gdb: is an object of the GraspDB class in the database package
        :return:

        author: weiwei
        date: 20170110
        """

        idhand = gdb.loadIdHand(self.handname)
        idobject = gdb.loadIdObject(self.objname)

        sql = "SELECT * FROM freeairgrip, object WHERE freeairgrip.idobject LIKE '%s' AND \
                freeairgrip.idhand LIKE '%s'" % (idobject, idhand)
        result = gdb.execute(sql)
        if len(result) > 0:
            print("Grasps already saved or duplicated filename!")
            isredo = input("Do you want to overwrite the database? (Y/N)")
            if isredo != "Y" and isredo != "y":
                print("Grasp planning aborted.")
            else:
                sql = "DELETE FROM freeairgrip WHERE freeairgrip.idobject LIKE '%s' AND \
                        freeairgrip.idhand LIKE '%s'"  % (idobject, idhand)
                gdb.execute(sql)
        sqlinsert = "INSERT INTO freeairgrip(idobject, contactpnt0, contactpnt1, contactnormal0, contactnormal1, rotmat, jawwidth, idhand) VALUES "
        for i in range(len(self.gripcontacts_planned)):
            sqlvalue = "('%s', '%s', '%s', '%s', '%s', '%s', '%s', %d)" % \
                      (idobject, dc.v3ToStr(self.gripcontacts_planned[i][0]), dc.v3ToStr(self.gripcontacts_planned[i][1]),
                       dc.v3ToStr(self.gripcontactnormals_planned[i][0]), dc.v3ToStr(self.gripcontactnormals_planned[i][1]),
                       dc.mat4ToStr(self.griprotmats_planned[i]), str(self.gripjawwidth_planned[i]), idhand)
            sqlinsert = sqlinsert+sqlvalue+","
        gdb.execute(sqlinsert[:-1])

    def showPlanningAnimation(self, discretesize=8, contactoffset = 10):
        """
        show an animation of the planning process

        :param discretesize: the number of hand orientations
        :return: delayTime

        author: weiwei
        date: 20161212tsukuba, 20190516osaka
        """

        rtq85plotlist = [[]]
        counter_facetpair = [0]
        counter_contactpair = [0]
        counter_discretizeangle = [0]

        def updateshow(rtq85plotlist, counter_facetpair, counter_contactpair, counter_discretizeangle,
                   facetpairs, gripcontactpairs, gripcontactpairnormals, hand, bcdc, objcm, task):

            # update plotlist
            if len(rtq85plotlist[0])>0:
                for rtq85plotnode in rtq85plotlist[0]:
                    rtq85plotnode.removeNode()
            rtq85plotlist[0] = []

            # all the discretizedangles of a contactpair are traversed, switch to a new contactpair
            if counter_discretizeangle[0] >= discretesize:
                counter_contactpair[0] += 1
                counter_discretizeangle[0] = 0
                task.again
            # all the contactpairs in a facet are traversed, switch to a new facetpair
            if counter_contactpair[0] >= len(gripcontactpairs[counter_facetpair[0]]):
                counter_facetpair[0] += 1
                counter_contactpair[0] = 0
                counter_discretizeangle[0] = 0
                task.again
            # all the facetpairs are traversed, startover
            if counter_facetpair[0] >= len(facetpairs):
                counter_facetpair[0] = 0
                counter_contactpair[0] = 0
                counter_discretizeangle[0] = 0
                task.again
            contactpair = gripcontactpairs[counter_facetpair[0]][counter_contactpair[0]]
            fingerdistance = np.linalg.norm((contactpair[0] - contactpair[1])) + contactoffset
            if fingerdistance > self.hand.jawwidthopen:
                counter_contactpair[0] += 1
                return task.again
            fingercenter = (contactpair[0] + contactpair[1]) / 2.0
            contactnormal0 = gripcontactpairnormals[counter_facetpair[0]][counter_contactpair[0]][0]
            contactnormal1 = gripcontactpairnormals[counter_facetpair[0]][counter_contactpair[0]][1]
            avgcontactnormal = rm.unitvec_safe(contactnormal0 - contactnormal1)[1]
            rotangle = 360.0 / discretesize * counter_discretizeangle[0]
            temphand = hand.copy()
            temphand.gripAt(fingercenter[0], fingercenter[1], fingercenter[2],
                        avgcontactnormal[0], avgcontactnormal[1], avgcontactnormal[2],
                        rotangle, fingerdistance)
            if self.__togglebcdcdebug:
                bcdc.unshow()
                bcdc.showMeshList(temphand.cmlist)
                bcdc.showMesh(objcm)
            result = bcdc.isMeshMeshListCollided(objcm, temphand.cmlist)

            # NOTE: for plot, only one side of the grasp is shown (fgr0 on contact0)
            # the symmetric 180 reverse grasp is ignored
            if not result:
                temphand.setColor(0,1,0,.5)
                temphand.reparentTo(base.render)
                rtq85plotlist[0].append(temphand)
            else:
                temphand.setColor(1,0,0,.5)
                temphand.reparentTo(base.render)
                rtq85plotlist[0].append(temphand)
            counter_discretizeangle[0] += 1
            return task.again

        taskMgr.doMethodLater(.1, updateshow, "updateshow",
                              extraArgs=[rtq85plotlist, counter_facetpair, counter_contactpair, counter_discretizeangle,
                                         self.facetpairs, self.gripcontactpairs, self.gripcontactpairnormals, self.hand,
                                         self.bcdc, self.objcm], appendTask=True)

class FreegripDB(object):
    """
    access data from db
    """

    def __init__(self, gdb, objname, hndname):

        freeairgripdata = gdb.loadFreeAirGrip(objname, hndname)
        if freeairgripdata is None:
            raise ValueError("Plan the freeairgrip first!")

        self.freegripids = freeairgripdata[0]
        self.freegripcontacts = freeairgripdata[1]
        self.freegripnormals = freeairgripdata[2]
        self.freegriprotmats = freeairgripdata[3]
        self.freegripjawwidth = freeairgripdata[4]

    def plotHands(self, nodepath, hndfa, rgba=None, offset=0.0):
        """
        plot the grasps loaded from the database to a nodepath

        :param render:
        :param hndfa:
        :param offset:
        :return:

        author: weiwei
        date: 20190525
        """

        for i, freegriprotmat in enumerate(self.freegriprotmats):
            hand = hndfa.genHand()
            hand.setColor(.3, 0, 0, .3)
            # plot offset
            newpos = freegriprotmat.getRow3(3) - freegriprotmat.getRow3(2) * offset
            freegriprotmat.setRow(3, newpos)
            hand.setMat(pandamat4=freegriprotmat)
            hand.setJawwidth(self.freegripjawwidth[i])
            if rgba is not None:
                hand.setColor(rgba)
            hand.reparentTo(nodepath)


if __name__=='__main__':
    import manipulation.grip.robotiq85.robotiq85 as hnd
    import environment.collisionmodel as cm
    from panda3d.core import GeomNode
    from panda3d.core import NodePath
    from panda3d.core import Vec4
    import database.dbaccess as db
    import random
    import trimesh as tm
    import matplotlib.pyplot as plt

    # base = pandactrl.World(camp=[700,300,700], lookatp=[0,0,100])
    base = pandactrl.World(camp=[700,-300,300], lookatp=[0,0,100])

    # objpath = "../objects/sandpart.stl"
    # objpath = "../objects/043screwdriver1000.stl"
    # objpath = "../objects/035drill1000.stl"
    # objpath = "../objects/043screwdriver1000.stl"
    # objpath = "../objects/025mug1000.stl"
    # objpath = "../objects/025mug3000.stl"
    # objpath = "../objects/025mug3000_tsdf.stl"
    # objpath = "../objects/035drill3000_tsdf.stl"
    # objpath = "../objects/072toyplanedrill3000_tsdf.stl"
    # objpath = "../objects/housing.stl"
    # objpath = "../objects/ttube.stl"
    objpath = "../objects/tro/bunny1072.stl"
    objcm = cm.CollisionModel(objinit = objpath)
    objcm.reparentTo(base.render)

    hndfa = hnd.Robotiq85Factory()
    hand= hndfa.genHand()
    freegriptst = Freegrip(objpath, hand, faceangle = .97, segangle = .9, refine1min=5, togglebcdcdebug=True, useoverlap=True)

    # geom = None
    facetsizes = []
    for i, faces in enumerate(freegriptst.facets):
        rgba = [np.random.random(),np.random.random(),np.random.random(),1]
        # compute facet normal
        facetnormal = np.sum(freegriptst.objtrimesh.face_normals[faces], axis=0)
        facetnormal = facetnormal/np.linalg.norm(facetnormal)
        geom = pandageom.packpandageom_fn(freegriptst.objtrimesh.vertices +
                                np.tile(0 * facetnormal, [freegriptst.objtrimesh.vertices.shape[0], 1]),
                                freegriptst.objtrimesh.face_normals[faces],
                                freegriptst.objtrimesh.faces[faces])
        node = GeomNode('piece')
        node.addGeom(geom)
        star = NodePath('piece')
        star.attachNewNode(node)
        star.setColor(Vec4(rgba[0],rgba[1],rgba[2],rgba[3]))
        star.setTwoSided(True)
        star.reparentTo(base.render)

        facetsizes.append(tm.Trimesh(vertices = freegriptst.objtrimesh.vertices, faces = freegriptst.objtrimesh.faces[faces],
                   face_normals = freegriptst.objtrimesh.face_normals[faces]).area)

    # plt.figure(figsize=(7, 2.4))
    # plt.plot(range(len(freegriptst.facets)), sorted(facetsizes))
    # plt.show()

    # freegriptst.showPlanningAnimation()
    # print("planning...")
    tic = time.time()
    freegriptst.planGrasps()
    toc = time.time()
    print("plan grasp cost", toc-tic)

    ngrasp = 0
    for pfacets in freegriptst.griprotmats_planned:
        for gmats in pfacets:
            ngrasp += len(gmats)
    nsamples = 0
    for i in range(len(freegriptst.facets)):
        for samples in freegriptst.objsamplepnts_refcls[i]:
            nsamples += len(samples)
    ncpairs = 0
    for pfacets in freegriptst.gripcontactpairs:
        for cpairs in pfacets:
            ncpairs += len(cpairs)
    print("number of grasps planned", ngrasp)
    print("number of samples", nsamples)
    print("number of contactpairs", ncpairs)

    for samples in freegriptst.objsamplepnts_refcls:
        for pnt in samples:
            base.pggen.plotSphere(base.render, pos=pnt, radius=4, rgba=[.7,0,0,1])

    def update(freegriptst, task):
        if base.inputmgr.keyMap['space'] is True:
            for i, pfacets in enumerate(freegriptst.gripcontactpairs):
                for j, contactpair in enumerate(pfacets):
                    cpt0 = contactpair[0]
                    nrmal0 = freegriptst.gripcontactpairnormals[i][j][0]
                    cpt1 = contactpair[1]
                    nrmal1 = freegriptst.gripcontactpairnormals[i][j][1]
                    base.pggen.plotSphere(base.render, pos=cpt0, radius=5, rgba=[.7,0,0,1])
                    base.pggen.plotArrow(base.render, spos=cpt0, epos=cpt0+nrmal0, length=10, thickness=2, rgba=[.7,0,0,1])
                    base.pggen.plotSphere(base.render, pos=cpt1, radius=5, rgba=[0,0,.7,1])
                    base.pggen.plotArrow(base.render, spos=cpt1, epos=cpt1+nrmal1, length=10, thickness=2, rgba=[0,0,.7,1])
            base.inputmgr.keyMap['space'] = False
        if base.inputmgr.keyMap['g'] is True:
            for i, freegriprotmat in enumerate(freegriptst.griprotmats_planned):
                print(freegriprotmat)
                hand = hndfa.genHand()
                hand.setColor(1,1,1,.3)
                newpos = freegriprotmat.getRow3(3)-freegriprotmat.getRow3(2)*0.0
                freegriprotmat.setRow(3, newpos)
                hand.setMat(pandamat4=freegriprotmat)
                hand.setJawwidth(freegriptst.gripjawwidth_planned[i])
                hand.reparentTo(base.render)
            base.inputmgr.keyMap['g'] = False
        return task.again

    taskMgr.doMethodLater(0.05, update, "update",
                          extraArgs=[freegriptst],
                          appendTask=True)


    # for i, freegriprotmat in enumerate(freegriptst.griprotmats_planned):
    #     hand = hndfa.genHand()
    #     hand.setColor(1,1,1,.1)
    #     newpos = freegriprotmat.getRow3(3)-freegriprotmat.getRow3(2)*0.0
    #     freegriprotmat.setRow(3, newpos)
    #     hand.setMat(pandamat4=freegriprotmat)
    #     hand.setJawwidth(freegriptst.gripjawwidth_planned[i])
    #     hand.reparentTo(base.render)
    #
    # for pnts in freegriptst.objsamplepnts_refcls:
    #     for pnt in pnts:
    #         base.pggen.plotSphere(base.render, pos=pnt, radius=5, rgba=[1,0,0,1])

    # gdb = db.GraspDB(database="nxt")
    # print("saving to database...")
    # freegriptst.saveToDB(gdb)
    #
    # print("loading from database...")
    # data = gdb.loadFreeAirGrip(objcm.name, 'rtq85')
    # if data:
    #     print("plotting...")
    #     freegripid, freegripcontacts, freegripnormals, freegriprotmats, freegripjawwidth = data
    #     print(len(freegripid))
    #     for i, freegriprotmat in enumerate(freegriprotmats):
    #         hand = hndfa.genHand()
    #         hand.setColor(.3,0,0,.3)
    #         newpos = freegriprotmat.getRow3(3)-freegriprotmat.getRow3(2)*0.0
    #         freegriprotmat.setRow(3, newpos)
    #         hand.setMat(pandamat4=freegriprotmat)
    #         hand.setJawwidth(freegripjawwidth[i])
    #         hand.reparentTo(base.render)

    base.run()