# TODO: reduce the dependency on panda3d

import math
import os
import copy
import numpy as np

import utiltools.robotmath as rm
import pandaplotutils.pandactrl as pandactrl
import pandaplotutils.pandageom as pandageom
from direct.showbase.ShowBase import ShowBase
from panda3d.bullet import BulletDebugNode
from panda3d.bullet import BulletRigidBodyNode
from panda3d.bullet import BulletTriangleMesh
from panda3d.bullet import BulletTriangleMeshShape
from panda3d.bullet import BulletWorld
from panda3d.core import *


class Hrp5pf3():
    '''
    use utils.designpattern.singleton() to get a single instance of this class

    the range of the hand is set to 0~82
    '''

    def __init__(self, jawwidth=160.0, hndid='lft', hndcolor = [.2,.2,.2,1]):
        '''
        load the hrp5pf3 model, set jawwidth and return a nodepath
        the hrp5pf3 gripper is composed of three 3-jnt fingers,
        the first jnt is assumed to be active
        its rotation range is set to 39.32~90 (angle between the inner surface of the finger and the palm)
        leading to 0~82 jawwidth

        ## input
        pandabase:
            the showbase() object
        jawwidth:
            the distance between the third jnts

        author: weiwei
        date: 20180203
        '''

        self.__hrp5pf3np = NodePath("hrp5pf3")
        self.__hrp5pf3thmnp = NodePath("hrp5pf3thm")
        self.__hrp5pf3idxmidnp = NodePath("hrp5pf3idxmid")
        self.__hrp5pf3fgrtippccnp = NodePath("hrp5pf3fgrtippcc")
        self.jawwidth = jawwidth
        self.hndid = hndid

        this_dir, this_filename = os.path.split(__file__)
        if hndid == 'lft':
            hrp5pf3basepath = Filename.fromOsSpecific(os.path.join(this_dir, "hrp5pf3egg", "lpalm.egg"))
            # thumb
            hrp5pf3thmj0 = Filename.fromOsSpecific(os.path.join(this_dir, "hrp5pf3egg", "lfgrthmj0.egg"))
            hrp5pf3thmj1 = Filename.fromOsSpecific(os.path.join(this_dir, "hrp5pf3egg", "lfgrthmj1.egg"))
            hrp5pf3thmj2 = Filename.fromOsSpecific(os.path.join(this_dir, "hrp5pf3egg", "lfgrthmj2.egg"))
            # idx
            hrp5pf3idxj0 = Filename.fromOsSpecific(os.path.join(this_dir, "hrp5pf3egg", "lfgridxj0.egg"))
            hrp5pf3idxj1 = Filename.fromOsSpecific(os.path.join(this_dir, "hrp5pf3egg", "lfgridxj1.egg"))
            hrp5pf3idxj2 = Filename.fromOsSpecific(os.path.join(this_dir, "hrp5pf3egg", "lfgridxj2.egg"))
            # middle
            hrp5pf3midj0 = Filename.fromOsSpecific(os.path.join(this_dir, "hrp5pf3egg", "lfgrmidj0.egg"))
            hrp5pf3midj1 = Filename.fromOsSpecific(os.path.join(this_dir, "hrp5pf3egg", "lfgrmidj1.egg"))
            hrp5pf3midj2 = Filename.fromOsSpecific(os.path.join(this_dir, "hrp5pf3egg", "lfgrmidj2.egg"))
        elif hndid == 'rgt':
            hrp5pf3basepath = Filename.fromOsSpecific(os.path.join(this_dir, "hrp5pf3egg", "rpalm.egg"))
            # thumb
            hrp5pf3thmj0 = Filename.fromOsSpecific(os.path.join(this_dir, "hrp5pf3egg", "rfgrthmj0.egg"))
            hrp5pf3thmj1 = Filename.fromOsSpecific(os.path.join(this_dir, "hrp5pf3egg", "rfgrthmj1.egg"))
            hrp5pf3thmj2 = Filename.fromOsSpecific(os.path.join(this_dir, "hrp5pf3egg", "rfgrthmj2.egg"))
            # idx
            hrp5pf3idxj0 = Filename.fromOsSpecific(os.path.join(this_dir, "hrp5pf3egg", "rfgridxj0.egg"))
            hrp5pf3idxj1 = Filename.fromOsSpecific(os.path.join(this_dir, "hrp5pf3egg", "rfgridxj1.egg"))
            hrp5pf3idxj2 = Filename.fromOsSpecific(os.path.join(this_dir, "hrp5pf3egg", "rfgridxj2.egg"))
            # middle
            hrp5pf3midj0 = Filename.fromOsSpecific(os.path.join(this_dir, "hrp5pf3egg", "rfgrmidj0.egg"))
            hrp5pf3midj1 = Filename.fromOsSpecific(os.path.join(this_dir, "hrp5pf3egg", "rfgrmidj1.egg"))
            hrp5pf3midj2 = Filename.fromOsSpecific(os.path.join(this_dir, "hrp5pf3egg", "rfgrmidj2.egg"))
        # finger tips
        thmfgrpccpath = Filename.fromOsSpecific(os.path.join(this_dir, "hrp5pf3egg", "thmtippcc.egg"))
        imfgrpccpath = Filename.fromOsSpecific(os.path.join(this_dir, "hrp5pf3egg", "imtippcc.egg"))

        # loader is a global variable defined by panda3d
        hrp5pf3_base = loader.loadModel(hrp5pf3basepath)
        hrp5pf3_thmj0 = loader.loadModel(hrp5pf3thmj0)
        hrp5pf3_thmj1 = loader.loadModel(hrp5pf3thmj1)
        self.__hrp5pf3_thmj2 = loader.loadModel(hrp5pf3thmj2)
        hrp5pf3_idxj0 = loader.loadModel(hrp5pf3idxj0)
        hrp5pf3_idxj1 = loader.loadModel(hrp5pf3idxj1)
        self.__hrp5pf3_idxj2 = loader.loadModel(hrp5pf3idxj2)
        hrp5pf3_midj0 = loader.loadModel(hrp5pf3midj0)
        hrp5pf3_midj1 = loader.loadModel(hrp5pf3midj1)
        self.__hrp5pf3_midj2 = loader.loadModel(hrp5pf3midj2)
        thmfgrtippcc = loader.loadModel(thmfgrpccpath)
        imfgrtippcc = loader.loadModel(imfgrpccpath)

        if hndid == 'rgt':
            # thm
            self.__hrp5pf3_thmj2.reparentTo(hrp5pf3_thmj1)
            self.__hrp5pf3_thmj2.setPos(0.0, 27.0, 0.0)
            hrp5pf3_thmj1.reparentTo(hrp5pf3_thmj0)
            hrp5pf3_thmj1.setPos(0.0, 46.0, 0.0)
            hrp5pf3_thmj0.reparentTo(self.__hrp5pf3thmnp)
            self.__hrp5pf3thmnp.setPos(0, 47.0, -114.0)
            # idx
            self.__hrp5pf3_idxj2.reparentTo(hrp5pf3_idxj1)
            self.__hrp5pf3_idxj2.setPos(0.0, -27.0, 0.0)
            hrp5pf3_idxj1.reparentTo(hrp5pf3_idxj0)
            hrp5pf3_idxj1.setPos(0.0, -46.0, 0.0)
            hrp5pf3_idxj0.reparentTo(self.__hrp5pf3idxmidnp)
            hrp5pf3_idxj0.setPos(30.0, 0.0, 0.0)
            self.__hrp5pf3idxmidnp.setPos(0.0, -47.0, -114.0)
            # mid
            self.__hrp5pf3_midj2.reparentTo(hrp5pf3_midj1)
            self.__hrp5pf3_midj2.setPos(0.0, -27.0, 0.0)
            hrp5pf3_midj1.reparentTo(hrp5pf3_midj0)
            hrp5pf3_midj1.setPos(0.0, -46.0, 0.0)
            hrp5pf3_midj0.reparentTo(self.__hrp5pf3idxmidnp)
            hrp5pf3_midj0.setPos(-30.0, 0.0, 0.0)
            self.__hrp5pf3idxmidnp.setPos(0.0, -47.0, -114.0)

        if hndid == 'lft':
            # thm
            self.__hrp5pf3_thmj2.reparentTo(hrp5pf3_thmj1)
            self.__hrp5pf3_thmj2.setPos(0.0, -27.0, 0.0)
            hrp5pf3_thmj1.reparentTo(hrp5pf3_thmj0)
            hrp5pf3_thmj1.setPos(0.0, -46.0, 0.0)
            hrp5pf3_thmj0.reparentTo(self.__hrp5pf3thmnp)
            self.__hrp5pf3thmnp.setPos(0, -47.0, -114.0)
            # idx
            self.__hrp5pf3_idxj2.reparentTo(hrp5pf3_idxj1)
            self.__hrp5pf3_idxj2.setPos(0.0, 27.0, 0.0)
            hrp5pf3_idxj1.reparentTo(hrp5pf3_idxj0)
            hrp5pf3_idxj1.setPos(0.0, 46.0, 0.0)
            hrp5pf3_idxj0.reparentTo(self.__hrp5pf3idxmidnp)
            hrp5pf3_idxj0.setPos(30.0, 0.0, 0.0)
            # mid
            self.__hrp5pf3_midj2.reparentTo(hrp5pf3_midj1)
            self.__hrp5pf3_midj2.setPos(0.0, 27.0, 0.0)
            hrp5pf3_midj1.reparentTo(hrp5pf3_midj0)
            hrp5pf3_midj1.setPos(0.0, 46.0, 0.0)
            hrp5pf3_midj0.reparentTo(self.__hrp5pf3idxmidnp)
            hrp5pf3_midj0.setPos(-30.0, 0.0, 0.0)
            self.__hrp5pf3idxmidnp.setPos(0.0, 47.0, -114.0)

        # transform the hand to standard pyhiro coordinate system
        hrp5pf3transpyhiro_np = NodePath("hrp5pf3base")
        rotmat41 = Mat4.rotateMat(180, Vec3(0,1,0))
        rotmat42 = Mat4.rotateMat(-90, Vec3(0,0,1))
        hrp5pf3transpyhiro_np.setMat(hrp5pf3transpyhiro_np.getMat()*rotmat41*rotmat42)

        # fgrnode
        hrp5pf3_base.reparentTo(hrp5pf3transpyhiro_np)
        self.__hrp5pf3thmnp.reparentTo(hrp5pf3transpyhiro_np)
        self.__hrp5pf3idxmidnp.reparentTo(hrp5pf3transpyhiro_np)
        hrp5pf3transpyhiro_np.setColor(hndcolor[0],hndcolor[1],hndcolor[2],hndcolor[3])
        hrp5pf3transpyhiro_np.reparentTo(self.__hrp5pf3np)

        # fgrtip node
        # thm pcc
        self.__thmfgrtippcc_np = NodePath("thmfgrtippcc")
        thmfgrtippcc.instanceTo(self.__thmfgrtippcc_np)
        thmtiprot = self.__hrp5pf3_thmj2.getMat(base.render)
        self.__thmfgrtippcc_np.setPos(thmtiprot.getRow3(3))
        # idxmid pcc
        self.__imfgrtippcc_np = NodePath("imfgrtippcc")
        idxfgrtippcc_np = NodePath("idxtippcc")
        midfgrtippcc_np = NodePath("midtippcc")
        imfgrtippcc.instanceTo(idxfgrtippcc_np)
        imfgrtippcc.instanceTo(midfgrtippcc_np)
        idxtiprot = self.__hrp5pf3_idxj2.getMat(base.render)
        midtiprot = self.__hrp5pf3_midj2.getMat(base.render)
        idxfgrtippcc_np.reparentTo(self.__imfgrtippcc_np)
        midfgrtippcc_np.reparentTo(self.__imfgrtippcc_np)
        idxfgrtippcc_np.setPos(Vec3(0.0,0.0,30.0))
        midfgrtippcc_np.setPos(Vec3(0.0,0.0,-30.0))
        self.__imfgrtippcc_np.setPos((idxtiprot.getRow3(3)+midtiprot.getRow3(3))/2.0)
        self.__thmfgrtippcc_np.reparentTo(self.__hrp5pf3fgrtippccnp)
        self.__imfgrtippcc_np.reparentTo(self.__hrp5pf3fgrtippccnp)

        self.__jawwidthopen = 160.0
        self.__jawwidthclosed = 0.0
        self.setJawwidth(jawwidth)

    @property
    def handnp(self):
        # read-only property
        return self.__hrp5pf3np

    @property
    def handpccnp(self):
        # read-only property
        return self.__hrp5pf3fgrtippccnp

    @property
    def jawwidthopen(self):
        # read-only property
        return self.__jawwidthopen

    @property
    def jawwidthclosed(self):
        # read-only property
        return self.__jawwidthclosed

    def setJawwidth(self, jawwidth):
        '''
        set the jawwidth of rtq85hnd
        the formulea is deduced on a note book
        the parallelism: 1.905-1.905; 5.715-5.715; 70/110 degree
        the triangle: 4.75 (finger) 5.715 (inner knuckle) 3.175 (outer knuckle)

        ## input
        rtq85hnd:
            nodepath of a robotiq85hand
        jawwidth:
            the width of the jaw

        author: weiwei
        date: 20160627
        '''
        assert (jawwidth <= 160.0)
        assert (jawwidth >= 0)

        self.jawwidth = jawwidth
        fgrangle = self.jawwidth2fgrangle()
        # right hand
        if self.hndid == "rgt":
            # thm
            rotmat4 = Mat4.rotateMat(-fgrangle, Vec3(1, 0, 0))
            rotmat4.setRow(3, self.__hrp5pf3thmnp.getMat().getRow3(3))
            self.__hrp5pf3thmnp.setMat(rotmat4)
            # idx and mid
            rotmat4 = Mat4.rotateMat(fgrangle, Vec3(1, 0, 0))
            rotmat4.setRow(3, self.__hrp5pf3idxmidnp.getMat().getRow3(3))
            self.__hrp5pf3idxmidnp.setMat(rotmat4)
        if self.hndid == "lft":
            # thm
            rotmat4 = Mat4.rotateMat(fgrangle, Vec3(1, 0, 0))
            rotmat4.setRow(3, self.__hrp5pf3thmnp.getMat().getRow3(3))
            self.__hrp5pf3thmnp.setMat(rotmat4)
            # idx and mid
            rotmat4 = Mat4.rotateMat(-fgrangle, Vec3(1, 0, 0))
            rotmat4.setRow(3, self.__hrp5pf3idxmidnp.getMat().getRow3(3))
            self.__hrp5pf3idxmidnp.setMat(rotmat4)
        thmtiprot = self.__hrp5pf3_thmj2.getMat(self.__hrp5pf3np)
        self.__thmfgrtippcc_np.setPos(thmtiprot.getRow3(3))
        idxtiprot = self.__hrp5pf3_idxj2.getMat(self.__hrp5pf3np)
        midtiprot = self.__hrp5pf3_midj2.getMat(self.__hrp5pf3np)
        self.__imfgrtippcc_np.setPos((idxtiprot.getRow3(3)+midtiprot.getRow3(3))/2.0)

    def genCloseRange(self, jawwidth, discretizedegree = 10.0):
        '''
        generate a hand model for collision.
        The finger motion is discretized in range (jawwidth, jawopen)
        The generated model is an independent copy, the hand itsself is not modified

        ## input
        rtq85hnd:
            nodepath of a robotiq85hand
        jawwidth:
            the width of the jaw

        author: weiwei
        date: 20160627
        '''

        assert (jawwidth <= 160.0)
        assert (jawwidth >= 0.0)

        originjawwidth = self.jawwidth
        rangenp = NodePath("rangemodel")
        print(np.arange(jawwidth, self.__jawwidthopen, discretizedegree))
        for rangejw in np.arange(jawwidth, self.__jawwidthopen+2*discretizedegree, discretizedegree):
            # print rangejw
            self.jawwidth = rangejw
            fgrangle = self.jawwidth2fgrangle()
            # print rangejw, fgrangle
            # right hand
            if self.hndid == "rgt":
                # thm
                rotmat4 = Mat4.rotateMat(-fgrangle, Vec3(1, 0, 0))
                rotmat4.setRow(3, self.__hrp5pf3thmnp.getMat().getRow3(3))
                self.__hrp5pf3thmnp.setMat(rotmat4)
                # idx and mid
                rotmat4 = Mat4.rotateMat(fgrangle, Vec3(1, 0, 0))
                rotmat4.setRow(3, self.__hrp5pf3idxmidnp.getMat().getRow3(3))
                self.__hrp5pf3idxmidnp.setMat(rotmat4)
            if self.hndid == "lft":
                # thm
                rotmat4 = Mat4.rotateMat(fgrangle, Vec3(1, 0, 0))
                rotmat4.setRow(3, self.__hrp5pf3thmnp.getMat().getRow3(3))
                self.__hrp5pf3thmnp.setMat(rotmat4)
                # idx and mid
                rotmat4 = Mat4.rotateMat(-fgrangle, Vec3(1, 0, 0))
                rotmat4.setRow(3, self.__hrp5pf3idxmidnp.getMat().getRow3(3))
                self.__hrp5pf3idxmidnp.setMat(rotmat4)
            tmphnd = copy.deepcopy(self.__hrp5pf3np)
            tmphnd.reparentTo(rangenp)
        self.setJawwidth(originjawwidth)
        return rangenp

    def setPos(self, pandanpvec3):
        """
        set the pose of the hand
        changes self.__hrp5threenp

        :param pandanpvec3
        :return:
        """

        self.__hrp5pf3np.setPos(pandanpvec3)

    def getPos(self):
        """
        get the pose of the hand

        :return:npvec3
        """

        return self.__hrp5pf3np.getPos()

    def setMat(self, nodepath, pandanpmat4):
        self.__hrp5pf3np.setMat(nodepath, pandanpmat4)

    def setMat(self, pandanpmat4=Mat4.identMat()):
        self.__hrp5pf3np.setMat(pandanpmat4)

    def getMat(self):
        """
        get the rotation matrix of the hand

        :return: pandanpmat4: follows panda3d, a LMatrix4f matrix

        date: 20161109
        author: weiwei
        """

        return self.__hrp5pf3np.getMat()

    def getHandName(self):
        return "hrp5pf3"

    def reparentTo(self, nodepath):
        """
        add to scene, follows panda3d

        :param nodepath: a panda3d nodepath
        :return: null

        date: 20161109
        author: weiwei
        """
        self.__hrp5pf3np.reparentTo(nodepath)

    def removeNode(self):
        """

        :return:
        """

        self.__hrp5pf3np.removeNode()

    def lookAt(self, direct0, direct1, direct2):
        """
        set the Y axis of the hnd

        author: weiwei
        date: 20161212
        """

        self.__hrp5pf3np.lookAt(direct0, direct1, direct2)
        self.__hrp5pf3fgrtippccnp.lookAt(direct0, direct1, direct2)

    def jawwidth2fgrangle(self, jawwidth=None):
        """
        convert jawwidth to the angle of fingers
        the value is the angle between the fingers and fully open (180degrees)

        :param jawwidth, if it was not None, return the correspodent angle of jawwidth,
                else, return the correspondent angle of self.jawwidth
        :return: angle in degree

        author: weiwe
        date: 20170316
        """

        degree = 0
        if jawwidth is not None:
            assert (jawwidth <= self.__jawwidthopen)
            assert (jawwidth >= self.__jawwidthclosed)
            degree =  180.0-math.degrees(math.acos(float(82.0 - jawwidth) / 149.0))
        else:
            degree =  180.0-math.degrees(math.acos(float(82.0 - self.jawwidth) / 149.0))
        if degree < 60.0:
            degree = 60.0
        if degree > 120.0:
            degree = 120.0
        return degree

    def gripAt(self, fcx, fcy, fcz, c0nx, c0ny, c0nz, rotangle=0, jawwidth=82):
        '''
        set the hand to grip at fcx, fcy, fcz, fc = finger center
        the normal of the sglfgr contact is set to be c0nx, c0ny, c0nz
        the rotation around the normal is set to rotangle
        the jawwidth is set to jawwidth

        date: 20170316
        author: weiwei
        '''

        self.__hrp5pf3np.setMat(Mat4.identMat())
        self.setJawwidth(jawwidth)
        if self.hndid == 'rgt':
            hndy = self.__hrp5pf3np.getMat().getRow3(1)
            hndx = Vec3(c0nx, c0ny, c0nz)
            hndz = hndx.cross(hndy)
            hndmat = Mat4(Mat3(hndx[0],hndx[1],hndx[2],hndy[0],hndy[1],hndy[2],hndz[0],hndz[1],hndz[2]))
            self.__hrp5pf3np.setMat(hndmat)
            self.__hrp5pf3fgrtippccnp.setMat(hndmat)
        elif self.hndid == 'lft':
            hndy = self.__hrp5pf3np.getMat().getRow3(1)
            hndx = Vec3(-c0nx, -c0ny, -c0nz)
            hndz = hndx.cross(hndy)
            hndmat = Mat4(Mat3(hndx[0],hndx[1],hndx[2],hndy[0],hndy[1],hndy[2],hndz[0],hndz[1],hndz[2]))
            self.__hrp5pf3np.setMat(hndmat)
            self.__hrp5pf3fgrtippccnp.setMat(hndmat)
        rotmat4x = Mat4.rotateMat(rotangle, Vec3(c0nx, c0ny, c0nz))
        # rot hand
        self.__hrp5pf3np.setMat(self.__hrp5pf3np.getMat()*rotmat4x)
        rotmat4 = Mat4(self.__hrp5pf3np.getMat())
        handtipvec3 = -rotmat4.getRow3(2) * (114.0+46.0+27.0)
        rotmat4.setRow(3, Vec3(fcx, fcy, fcz) + handtipvec3)
        self.__hrp5pf3np.setMat(rotmat4)
        # rot pcc
        self.__hrp5pf3fgrtippccnp.setMat(self.__hrp5pf3fgrtippccnp.getMat()*rotmat4x)
        rotmat4 = Mat4(self.__hrp5pf3fgrtippccnp.getMat())
        handtipvec3 = -rotmat4.getRow3(2) * (114.0+46.0+27.0)
        rotmat4.setRow(3, Vec3(fcx, fcy, fcz) + handtipvec3)
        self.__hrp5pf3fgrtippccnp.setMat(rotmat4)

    def setColor(self, rgbacolor=[1,0,0,.1]):
        nodepathlist = self.__hrp5pf3np.getChildren()
        for nodepathi in nodepathlist:
            # nodepathi.clearMaterial()
            nodepathi.setColor(rgbacolor[0], rgbacolor[1], rgbacolor[2], rgbacolor[3])

def newHand(hndid='rgt', jawwidth=160.0):
    return Hrp5pf3(jawwidth=jawwidth, hndid=hndid)

def newHandNM(hndid = 'rgt', jawwidth = 160.0, hndcolor = [.2,.2,.2,1]):
    return Hrp5pf3(jawwidth= jawwidth, hndid = hndid, hndcolor=Vec4(hndcolor[0], hndcolor[1], hndcolor[2], hndcolor[3]))

def newHandFgrpcc():
    this_dir, this_filename = os.path.split(__file__)
    # single finger
    handfgrpccpath = Filename.fromOsSpecific(os.path.join(this_dir, "hrp5pf3egg", "Precc.egg"))
    handfgrpcc = loader.loadModel(handfgrpccpath)
    return handfgrpcc

def getHandName():
    return "hrp5pf3"

if __name__ == '__main__':
    import utiltools.collisiondetection as cd
    import pandaplotutils.pandageom as pg

    def updateworld(world, task):
        world.doPhysics(globalClock.getDt())
        # result = base.world.contactTestPair(bcollidernp.node(), lftcollidernp.node())
        # result1 = base.world.contactTestPair(bcollidernp.node(), ilkcollidernp.node())
        # result2 = base.world.contactTestPair(lftcollidernp.node(), ilkcollidernp.node())
        # print result
        # print result.getContacts()
        # print result1
        # print result1.getContacts()
        # print result2
        # print result2.getContacts()
        # for contact in result.getContacts():
        #     cp = contact.getManifoldPoint()
        #     print cp.getLocalPointA()
        return task.cont


    # base = pandactrl.World( camp=[40000,10000,40000]
    # )
    base = pandactrl.World()
    # first hand
    hrp5pf3rgt = newHand(hndid='rgt')
    hrp5pf3rgt.setJawwidth(hrp5pf3rgt.jawwidthopen/2.0)
    hrp5pf3rgt.gripAt(10, 25, 0, 1, 0, 0, 30, jawwidth=30)
    hrp5pf3rgt.reparentTo(base.render)
    base.pggen.plotAxis(base.render)
    base.run()
    hrp5pf3rgt.setMat(Mat4.identMat())
    hrp5pf3rgt.setJawwidth(hrp5pf3rgt.jawwidthopen)
    g0 = hrp5pf3rgt.genCloseRange(20)
    g0.reparentTo(base.render)
    # hrp5pf3rgt.reparentTo(base.render)
    # hrp5pf3rgt.setJawwidth(80)
    hrp5pf3rgt1 = newHandNM(hndid='rgt', hndcolor=[1,1,0,1])
    # hrp5pf3rgt1.setJawwidth(hrp5pf3rgt.jawwidthopen)
    hrp5pf3rgt1.gripAt(10, -20, 0, -1, 0, 0, 0, jawwidth=30)
    g1 = hrp5pf3rgt1.genCloseRange(30)
    g1.reparentTo(base.render)
    # hrp5pf3rgt1.reparentTo(base.render)
    # hrp5pf3 = newHand(hndid='lft')
    # hrp5pf3.gripAt(100, 0, 0, 0, 0, 1, 35, jawwidth=30)
    # hrp5pf3.reparentTo(base.render)
    # hrp5three.handpccnp.reparentTo(base.render)
    # second hand
    # hrp5three1 = newHand(hndid='rgt')
    # hrp5three1.reparentTo(base.render)
    # hrp5three1.fgrtippccleft.reparentTo(base.render)
    # hrp5three1.fgrtippccright.reparentTo(base.render)
    # hand1bullnp = cd.genCollisionMeshNp(hrp5pf3.handnp)

    pggen = pg.PandaGeomGen()
    pggen.plotAxis(base.render)

    bullcldrnp = base.render.attachNewNode("bulletcollider")
    base.world = BulletWorld()
    hndbullnodei0 = cd.genCollisionMeshMultiNp(g0, base.render)
    hndbullnodei1 = cd.genCollisionMeshMultiNp(g1, base.render)
    result = base.world.contactTestPair(hndbullnodei0, hndbullnodei1)
    print(result.getNumContacts())
    if not result.getNumContacts():
        print("no contact")
    else:
        print("in collision")

    base.taskMgr.add(updateworld, "updateworld", extraArgs=[base.world], appendTask=True)
    base.world.attachRigidBody(hndbullnodei0)
    base.world.attachRigidBody(hndbullnodei1)
    # result = base.world.contactTestPair(handbullnp, hand1bullnp)
    # for contact in result.getContacts():
    #     cp = contact.getManifoldPoint()
    #     print cp.getLocalPointA()
    #     pg.plotSphere(base.render, pos=cp.getLocalPointA(), radius=1, rgba=Vec4(1,0,0,1))
    #
    debugNode = BulletDebugNode('Debug')
    debugNode.showWireframe(True)
    debugNode.showConstraints(True)
    debugNode.showBoundingBoxes(False)
    debugNode.showNormals(False)
    debugNP = bullcldrnp.attachNewNode(debugNode)
    # debugNP.show()

    base.world.setDebugNode(debugNP.node())

    base.run()