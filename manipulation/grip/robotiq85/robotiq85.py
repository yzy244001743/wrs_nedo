import os
import math

import utiltools.robotmath as rm
import pandaplotutils.pandactrl as pandactrl
from panda3d.core import *
import numpy as np
from environment import collisionmodel as cm
import trimesh.primitives as tp
import copy as cp

class Robotiq85(object):
    """
    use utiltools.designpattern.singleton() to get a single instance of this class
    warning: avoid using copy.deepcopy to copy an instance of this class since copy.deepcopy cannot maintain correct transformation
    """

    def __init__(self, *args, **kwargs):
        """
        load the robotiq85 model, set jawwidth and return a nodepath
        the rtq85 gripper is composed of a parallelism and a fixed triangle,
        the parallelism: 1.905-1.905; 5.715-5.715; 70/110 degree
        the triangle: 4.75 (finger) 5.715 (inner knuckle) 3.175 (outer knuckle)

        :param args:
        :param kwargs:
            'jawwidth' 0-85
            'ftsensoroffset' the offset for forcesensor
            'togglefrmes' True, False

        author: weiwei
        date: 20160627, 20190518
        """

        if 'jawwidth' in kwargs:
            self.__jawwidth = kwargs['jawwidth']
        if 'ftsensoroffset' in kwargs:
            self.__ftsensoroffset = kwargs['ftsensoroffset']
        if 'toggleframes' in kwargs:
            self.__toggleframes = kwargs['toggleframes']
        if 'rtq85base' in kwargs:
            self.__rtq85base = cp.deepcopy(kwargs['rtq85base'])
            # for copy
            self.__rtq85base_bk = cp.deepcopy(kwargs['rtq85base'])
        if 'rtq85iknuckle' in kwargs:
            self.__rtq85ilknuckle = cp.deepcopy(kwargs['rtq85iknuckle'])
            self.__rtq85irknuckle = cp.deepcopy(kwargs['rtq85iknuckle'])
            # for copy
            self.__rtq85iknuckle_bk = cp.deepcopy(kwargs['rtq85iknuckle'])
        if 'rtq85knuckle' in kwargs:
            self.__rtq85lknuckle = cp.deepcopy(kwargs['rtq85knuckle'])
            self.__rtq85rknuckle = cp.deepcopy(kwargs['rtq85knuckle'])
            # for copy
            self.__rtq85knuckle_bk = cp.deepcopy(kwargs['rtq85knuckle'])
        if 'rtq85fgr' in kwargs:
            self.__rtq85lfgr = cp.deepcopy(kwargs['rtq85fgr'])
            self.__rtq85rfgr = cp.deepcopy(kwargs['rtq85fgr'])
            # for copy
            self.__rtq85fgr_bk = cp.deepcopy(kwargs['rtq85fgr'])
        if 'rtq85fgrtip' in kwargs:
            self.__rtq85lfgrtip = cp.deepcopy(kwargs['rtq85fgrtip'])
            self.__rtq85rfgrtip = cp.deepcopy(kwargs['rtq85fgrtip'])
            # for copy
            self.__rtq85fgrtip_bk = cp.deepcopy(kwargs['rtq85fgrtip'])

        self.__name = "rtq85"
        self.__rtq85np = NodePath("rtq85hnd")
        self.__eetip = np.array([0.0, 0.0, 145.0])+np.array([0.0, 0.0, self.__ftsensoroffset])
        self.__jawwidthopen = 85.0
        self.__jawwidthclosed = 0.0

        # base
        self.__rtq85base.setPos(0.0, 0.0, self.__ftsensoroffset)

        # ftsensor
        if self.__ftsensoroffset > 0:
            self.__ftsensor = cm.CollisionModel(tp.Cylinder(height=self.__ftsensoroffset, radius=30), name="ftsensor")
            self.__ftsensor.setPos(0,0,-self.__ftsensoroffset/2)
            # ftsensor.setRPY(90,0,0)
            self.__ftsensor.reparentTo(self.__rtq85base)
        else:
            self.__ftsensor = None

        # left and right outer knuckle
        self.__rtq85lknuckle.setPos(30.60114443, 0, 54.90451627)
        self.__rtq85lknuckle.setRPY(0, 7.17, 0)
        self.__rtq85lknuckle.reparentTo(self.__rtq85base)
        self.__rtq85rknuckle.setPos(-30.60114443, 0, 54.90451627)
        self.__rtq85rknuckle.setRPY(0, 7.17, 180)
        self.__rtq85rknuckle.reparentTo(self.__rtq85base)

        # left and right inner knuckle
        self.__rtq85ilknuckle.setPos(12.7, 0, 61.42)
        self.__rtq85ilknuckle.setRPY(0, 41.14, 0)
        self.__rtq85ilknuckle.reparentTo(self.__rtq85base)
        self.__rtq85irknuckle.setPos(-12.7, 0, 61.42)
        self.__rtq85irknuckle.setRPY(0, 41.14, 180)
        self.__rtq85irknuckle.reparentTo(self.__rtq85base)

        # left and right finger
        self.__rtq85lfgr.setPos(31.48504435, 0, -4.08552455)
        self.__rtq85lfgr.reparentTo(self.__rtq85lknuckle)
        self.__rtq85rfgr.setPos(31.48504435, 0, -4.08552455)
        self.__rtq85rfgr.reparentTo(self.__rtq85rknuckle)

        # left and right fgr tip
        self.__rtq85lfgrtip.setPos(37.59940821, 0, 43.03959807)
        self.__rtq85lfgrtip.setPos(0, 0, 57.2)
        self.__rtq85lfgrtip.setRPY(0, -41.14, 0)
        self.__rtq85lfgrtip.reparentTo(self.__rtq85ilknuckle)
        self.__rtq85rfgrtip.setPos(37.59940821, 0, 43.03959807)
        self.__rtq85rfgrtip.setPos(0, 0, 57.2)
        self.__rtq85rfgrtip.setRPY(0, -41.14, 0)
        self.__rtq85rfgrtip.reparentTo(self.__rtq85irknuckle)

        # rotate to x, y, z coordinates (this one rotates the base, not the self.rtq85np)
        # the default x direction is facing the ee, the default z direction is facing downward
        # execute this file to see the default pose
        self.__rtq85base.reparentTo(self.__rtq85np)

        self.setJawwidth(self.__jawwidth)

        # default color
        self.setDefaultColor()

        if self.__toggleframes:
            if self.__ftsensor is not None:
                self.__ftsensorframe = base.pggen.genAxis()
                self.__ftsensorframe.reparentTo(self.__rtq85np)
            self.__rtq85hndframe = base.pggen.genAxis()
            self.__rtq85hndframe.reparentTo(self.__rtq85np)
            self.__rtq85baseframe = base.pggen.genAxis()
            self.__rtq85baseframe.reparentTo(self.__rtq85base.objnp)
            self.__rtq85ilknuckleframe = base.pggen.genAxis()
            self.__rtq85ilknuckleframe.reparentTo(self.__rtq85ilknuckle.objnp)
            self.__rtq85lknuckleframe = base.pggen.genAxis()
            self.__rtq85lknuckleframe.reparentTo(self.__rtq85lknuckle.objnp)
            self.__rtq85lfgrframe = base.pggen.genAxis()
            self.__rtq85lfgrframe.reparentTo(self.__rtq85lfgr.objnp)
            self.__rtq85lfgrtipframe = base.pggen.genAxis()
            self.__rtq85lfgrtipframe.reparentTo(self.__rtq85lfgrtip.objnp)
            self.__rtq85irknuckleframe = base.pggen.genAxis()
            self.__rtq85irknuckleframe.reparentTo(self.__rtq85irknuckle.objnp)
            self.__rtq85rknuckleframe = base.pggen.genAxis()
            self.__rtq85rknuckleframe.reparentTo(self.__rtq85rknuckle.objnp)
            self.__rtq85rfgrframe = base.pggen.genAxis()
            self.__rtq85rfgrframe.reparentTo(self.__rtq85rfgr.objnp)
            self.__rtq85rfgrtipframe = base.pggen.genAxis()
            self.__rtq85rfgrtipframe.reparentTo(self.__rtq85rfgrtip.objnp)

    @property
    def handnp(self):
        # read-only property
        return self.__rtq85np

    @property
    def jawwidthopen(self):
        # read-only property
        return self.__jawwidthopen

    @property
    def jawwidthclosed(self):
        # read-only property
        return self.__jawwidthclosed

    @property
    def jawwidth(self):
        # read-only property
        return self.__jawwidth

    @property
    def cmlist(self):
        # read-only property
        if self.__ftsensor is not None:
            return [self.__ftsensor, self.__rtq85base, self.__rtq85irknuckle, self.__rtq85rknuckle, self.__rtq85rfgr,
                    self.__rtq85rfgrtip, self.__rtq85ilknuckle, self.__rtq85lknuckle, self.__rtq85lfgr, self.__rtq85lfgrtip]
        else:
            return [self.__rtq85base, self.__rtq85irknuckle, self.__rtq85rknuckle, self.__rtq85rfgr, self.__rtq85rfgrtip,
                    self.__rtq85ilknuckle, self.__rtq85lknuckle, self.__rtq85lfgr, self.__rtq85lfgrtip]

    @property
    def eetip(self):
        # read-only property
        return self.__eetip

    @property
    def name(self):
        # read-only property
        return self.__name

    def setJawwidth(self, jawwidth):
        """
        set the jawwidth of rtq85hnd
        the rtq85 gripper is composed of a parallelism and a fixed triangle,
        the parallelism: 1.905-1.905; 5.715-5.715; 70/110 degree
        the triangle: 4.75 (finger) 5.715 (inner knuckle) 3.175 (outer knuckle)

        :param jawwidth: mm
        :return:

        author: weiwei
        date: 20160627, 20190514
        """

        if jawwidth > 85.0 or jawwidth < 0.0:
            print ("Wrong value! Jawwidth must be in (0.0,85.0). The input is "+str(jawwidth)+".")
            raise Exception("Jawwidth out of range!")

        rotiknuckle=math.degrees(math.asin((jawwidth/2-5)/57.15))

        # right finger
        rtq85irknucklerpy = self.__rtq85irknuckle.getRPY()
        self.__rtq85irknuckle.setRPY(rtq85irknucklerpy[0], rotiknuckle, rtq85irknucklerpy[2])
        rtq85rknucklerpy = self.__rtq85rknuckle.getRPY()
        self.__rtq85rknuckle.setRPY(rtq85rknucklerpy[0], rotiknuckle-33.78, rtq85rknucklerpy[2])
        rtq85rfgrtiprpy = self.__rtq85rfgrtip.getRPY()
        self.__rtq85rfgrtip.setRPY(rtq85rfgrtiprpy[0], -rotiknuckle, rtq85rfgrtiprpy[2])

        # left finger
        rtq85ilknucklerpy = self.__rtq85ilknuckle.getRPY()
        self.__rtq85ilknuckle.setRPY(rtq85ilknucklerpy[0], rotiknuckle, rtq85ilknucklerpy[2])
        rtq85lknucklerpy = self.__rtq85lknuckle.getRPY()
        self.__rtq85lknuckle.setRPY(rtq85lknucklerpy[0], rotiknuckle-33.78, rtq85lknucklerpy[2])
        rtq85lfgrtiprpy = self.__rtq85lfgrtip.getRPY()
        self.__rtq85lfgrtip.setRPY(rtq85lfgrtiprpy[0], -rotiknuckle, rtq85lfgrtiprpy[2])

        # update the private member variable
        self.__jawwidth = jawwidth

    def setPos(self, pandavec3):
        """
        set the pose of the hand
        changes self.rtq85np

        :param pandavec3 panda3d vec3
        :return:
        """

        self.__rtq85np.setPos(pandavec3)

    def getPos(self):
        """
        set the pose of the hand
        changes self.rtq85np

        :param pandavec3 panda3d vec3
        :return:
        """

        return self.__rtq85np.getPos()

    def setMat(self, pandamat4):
        """
        set the translation and rotation of a robotiq hand
        changes self.rtq85np

        :param pandamat4 panda3d Mat4
        :return: null

        date: 20161109
        author: weiwei
        """

        self.__rtq85np.setMat(pandamat4)

    def getMat(self):
        """
        get the rotation matrix of the hand

        :return: pandamat4 follows panda3d, a LMatrix4f matrix

        date: 20161109
        author: weiwei
        """

        return self.__rtq85np.getMat()

    def reparentTo(self, nodepath):
        """
        add to scene, follows panda3d

        :param nodepath: a panda3d nodepath
        :return: null

        date: 20161109
        author: weiwei
        """
        self.__rtq85np.reparentTo(nodepath)

    def removeNode(self):
        """

        :return:
        """

        self.__rtq85np.removeNode()

    def __lookAt(self, direct0, direct1, direct2):
        """
        set the Y axis of the hnd
        ** deprecated 20190517

        author: weiwei
        date: 20161212
        """

        self.__rtq85np.lookAt(direct0, direct1, direct2)

    def gripAt(self, fcx, fcy, fcz, c0nx, c0ny, c0nz, rotangle = 0, jawwidth = 82):
        """
        set the hand to grip at fcx, fcy, fcz, fc = finger center
        the normal of the sglfgr contact is set to be c0nx, c0ny, c0nz
        the rotation around the normal is set to rotangle
        the jawwidth is set to jawwidth

        date: 20170322
        author: weiwei
        """

        # x is the opening direction of the hand, _org means the value when rotangle=0
        standardx_org = np.array([1,0,0])
        newx_org = np.array([c0nx, c0ny, c0nz])
        rotangle_org = rm.degree_betweenvector(newx_org, standardx_org)
        rotaxis_org = np.array([0,0,1])
        if not (np.isclose(rotangle_org, 180.0) or np.isclose(rotangle_org, 0.0)):
            rotaxis_org = rm.unitvec_safe(np.cross(standardx_org, newx_org))[1]
        newrotmat_org = rm.rodrigues(rotaxis_org, rotangle_org)

        # rotate to the given rotangle
        hnd_rotmat4 = np.eye(4)
        hnd_rotmat4[:3,:3] = np.dot(rm.rodrigues(newx_org, rotangle), newrotmat_org)
        handtipnpvec3 = np.array([fcx, fcy, fcz])-np.dot(hnd_rotmat4[:3,:3], self.__eetip)
        hnd_rotmat4[:3,3] = handtipnpvec3
        self.__rtq85np.setMat(base.pg.np4ToMat4(hnd_rotmat4))
        self.setJawwidth(jawwidth)

        return [jawwidth, np.array([fcx, fcy, fcz]), hnd_rotmat4]

    def approachAt(self, fcx, fcy, fcz, c0nx, c0ny, c0nz, apx, apy, apz, jawwidth = 82):
        """
        set the hand to grip at fcx, fcy, fcz, fc = finger center
        the normal of the sglfgr contact is set to be c0nx, c0ny, c0nz
        the approach vector of the hand is set to apx, apy, apz
        the jawwidth is set to jawwidth

        date: 20190528
        author: weiwei
        """

        # x is the opening direction of the hand, _org means the value when rotangle=0
        nphndmat3 = np.eye(3)
        nphndmat3[:3,0] = rm.unitvec_safe(np.array([c0nx, c0ny, c0nz]))[1]
        nphndmat3[:3,2] = rm.unitvec_safe(np.array([apx, apy, apz]))[1]
        nphndmat3[:3,1] = rm.unitvec_safe(np.cross(np.array([apx, apy, apz]), np.array([c0nx, c0ny, c0nz])))[1]

        # rotate to the given rotangle
        hnd_rotmat4 = np.eye(4)
        hnd_rotmat4[:3,:3] = nphndmat3
        handtipnpvec3 = np.array([fcx, fcy, fcz])-np.dot(nphndmat3, self.__eetip)
        hnd_rotmat4[:3,3] = handtipnpvec3
        self.__rtq85np.setMat(base.pg.np4ToMat4(hnd_rotmat4))
        self.setJawwidth(jawwidth)

        return [jawwidth, np.array([fcx, fcy, fcz]), hnd_rotmat4]

    def setColor(self, *args, **kwargs):
        """

        :param rgba
        :return:

        author: weiwei
        date: 20190514
        """

        if self.__ftsensor is not None:
            self.__ftsensor.clearColor()
        self.__rtq85base.clearColor()
        self.__rtq85irknuckle.clearColor()
        self.__rtq85ilknuckle.clearColor()
        self.__rtq85rfgr.clearColor()
        self.__rtq85lfgr.clearColor()
        self.__rtq85rknuckle.clearColor()
        self.__rtq85lknuckle.clearColor()
        self.__rtq85rfgrtip.clearColor()
        self.__rtq85lfgrtip.clearColor()
        if len(args)==1:
            self.__rtq85np.setColor(args[0])
        else:
            # print(args[0], args[1], args[2], args[3])
            self.__rtq85np.setColor(args[0], args[1], args[2], args[3])

    def setDefaultColor(self, rgba_knucklefingertip=[.5,.5,.5,1], rgba_baseinnerknucklefinger=[.2,.2,.2,1]):
        """

        :param rgba_knucklefingertip:
        :param rgba_baseinnerknucklefinger:
        :return:

        author: weiwei
        date: 20190514
        """

        if self.__ftsensor is not None:
            self.__ftsensor.setColor(rgba_baseinnerknucklefinger[0], rgba_baseinnerknucklefinger[1], rgba_baseinnerknucklefinger[2], rgba_baseinnerknucklefinger[3])
        self.__rtq85base.setColor(rgba_baseinnerknucklefinger[0], rgba_baseinnerknucklefinger[1], rgba_baseinnerknucklefinger[2], rgba_baseinnerknucklefinger[3])
        self.__rtq85irknuckle.setColor(rgba_baseinnerknucklefinger[0], rgba_baseinnerknucklefinger[1], rgba_baseinnerknucklefinger[2], rgba_baseinnerknucklefinger[3])
        self.__rtq85ilknuckle.setColor(rgba_baseinnerknucklefinger[0], rgba_baseinnerknucklefinger[1], rgba_baseinnerknucklefinger[2], rgba_baseinnerknucklefinger[3])
        self.__rtq85rfgr.setColor(rgba_baseinnerknucklefinger[0], rgba_baseinnerknucklefinger[1], rgba_baseinnerknucklefinger[2], rgba_baseinnerknucklefinger[3])
        self.__rtq85lfgr.setColor(rgba_baseinnerknucklefinger[0], rgba_baseinnerknucklefinger[1], rgba_baseinnerknucklefinger[2], rgba_baseinnerknucklefinger[3])
        self.__rtq85rknuckle.setColor(rgba_knucklefingertip[0], rgba_knucklefingertip[1], rgba_knucklefingertip[2], rgba_knucklefingertip[3])
        self.__rtq85lknuckle.setColor(rgba_knucklefingertip[0], rgba_knucklefingertip[1], rgba_knucklefingertip[2], rgba_knucklefingertip[3])
        self.__rtq85rfgrtip.setColor(rgba_knucklefingertip[0], rgba_knucklefingertip[1], rgba_knucklefingertip[2], rgba_knucklefingertip[3])
        self.__rtq85lfgrtip.setColor(rgba_knucklefingertip[0], rgba_knucklefingertip[1], rgba_knucklefingertip[2], rgba_knucklefingertip[3])

    def copy(self):
        """
        make a copy of oneself
        use this one to copy a hand since copy.deepcopy doesnt work well with the collisionnode

        :return:

        author: weiwei
        date: 20190525osaka
        """

        hand = Robotiq85(jawwidth=self.__jawwidth, ftsensoroffset=self.__ftsensoroffset, toggleframes=self.__toggleframes,
                         rtq85base=self.__rtq85base_bk, rtq85iknuckle=self.__rtq85iknuckle_bk, rtq85knuckle=self.__rtq85knuckle_bk,
                         rtq85fgr=self.__rtq85fgr_bk, rtq85fgrtip=self.__rtq85fgrtip_bk)
        # hand.setColor(self.__rtq85np.getColor())
        return hand

    def showcn(self):
        self.__rtq85base.showcn()
        self.__rtq85irknuckle.showcn()
        self.__rtq85ilknuckle.showcn()
        self.__rtq85rfgr.showcn()
        self.__rtq85lfgr.showcn()
        self.__rtq85rknuckle.showcn()
        self.__rtq85lknuckle.showcn()
        self.__rtq85rfgrtip.showcn()
        self.__rtq85lfgrtip.showcn()

class Robotiq85Factory(object):

    def __init__(self):
        this_dir, this_filename = os.path.split(__file__)
        self.__rtq85base = cm.CollisionModel(objinit=this_dir+"/robotiq85stl/robotiq_85_base_link.stl", betransparency=True)
        self.__rtq85iknuckle = cm.CollisionModel(objinit=this_dir+"/robotiq85stl/robotiq_85_inner_knuckle_link.stl", betransparency=True)
        self.__rtq85knuckle = cm.CollisionModel(objinit=this_dir+"/robotiq85stl/robotiq_85_knuckle_link.stl", betransparency=True)
        self.__rtq85fgr = cm.CollisionModel(objinit=this_dir+"/robotiq85stl/robotiq_85_finger_link.stl", betransparency=True)
        self.__rtq85fgrtip = cm.CollisionModel(objinit=this_dir+"/robotiq85stl/robotiq_85_finger_tip_link.stl", betransparency=True)

    def genHand(self, jawwidth=85, ftsensoroffset = 52, toggleframes=False):
        return Robotiq85(jawwidth=jawwidth, ftsensoroffset=ftsensoroffset, toggleframes=toggleframes,
                         rtq85base=self.__rtq85base, rtq85iknuckle=self.__rtq85iknuckle, rtq85knuckle=self.__rtq85knuckle,
                         rtq85fgr=self.__rtq85fgr, rtq85fgrtip=self.__rtq85fgrtip)

if __name__=='__main__':
    import copy as cp
    import environment.bulletcdhelper as bcd

    base = pandactrl.World(lookatp=[0,0,0])
    base.pggen.plotAxis(base.render)

    rfa = Robotiq85Factory()
    rtq85hnd = rfa.genHand(toggleframes=True)
    rtq85hnd.gripAt(100,0,0,0,1,0,30,5)
    rtq85hnd.reparentTo(base.render)
    # rtq85hnd.showcn()

    rtq85hnd2 = rfa.genHand()
    rtq85hnd2.gripAt(-40, -18, 33, -0.9, 0.4, 0.0,60,13.204926835369958)
    # rtq85hnd2.setColor(1,0,0,.2)
    rtq85hnd2.reparentTo(base.render)
    rtq85hnd3 = rtq85hnd2.copy()
    rtq85hnd3.gripAt(0,0,0, 0,1,0,90,45)
    rtq85hnd3.reparentTo(base.render)
    # rtq85hnd2.showcn()

    bmc = bcd.MCMchecker(toggledebug=True)
    print(bmc.isMeshListMeshListCollided(rtq85hnd.cmlist, rtq85hnd2.cmlist))
    bmc.showMeshList(rtq85hnd.cmlist)
    bmc.showMeshList(rtq85hnd2.cmlist)
    bmc.showMeshList(rtq85hnd3.cmlist)
    bmc.showBoxList(rtq85hnd.cmlist)
    # bmc.showBoxList(rtq85hnd2.handnp)
    base.run()