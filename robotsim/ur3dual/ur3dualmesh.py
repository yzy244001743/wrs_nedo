import os

import numpy as np
import copy
from panda3d.core import NodePath, TransparencyAttrib

class Ur3DualMesh(object):
    """
    generate ur3dualmesh

    NOTE: it is unnecessary to attach a mnp to render repeatedly
    once attached, it is always there. update the joint angles
    will change the attached model directly
    """

    def __init__(self):
        """
        author: weiwei
        date: 20171213
        """

        ##########
        ### load the model files of the robots
        ##########
        this_dir, this_filename = os.path.split(__file__)

        robotwaist_filepath = os.path.join(this_dir, "ur3stl", "ur3dualbody.stl")

        ur3base_filepath = os.path.join(this_dir, "ur3stl", "base.stl")
        ur3upperarm_filepath = os.path.join(this_dir, "ur3stl", "upperarm.stl")
        ur3shoulder_filepath = os.path.join(this_dir, "ur3stl", "shoulder.stl")
        ur3forearm_filepath = os.path.join(this_dir, "ur3stl", "forearm.stl")
        ur3wrist1_filepath = os.path.join(this_dir, "ur3stl", "wrist1.stl")
        ur3wrist2_filepath = os.path.join(this_dir, "ur3stl", "wrist2.stl")
        ur3wrist3_filepath = os.path.join(this_dir, "ur3stl", "wrist3.stl")

        robotwaist_model = base.pg.loadstlaspandanp_fn(robotwaist_filepath)

        ur3base_model  = base.pg.loadstlaspandanp_fn(ur3base_filepath)
        ur3upperarm_model  = base.pg.loadstlaspandanp_fn(ur3upperarm_filepath)
        ur3shoulder_model = base.pg.loadstlaspandanp_fn(ur3shoulder_filepath)
        ur3forearm_model = base.pg.loadstlaspandanp_fn(ur3forearm_filepath)
        ur3wrist1_model = base.pg.loadstlaspandanp_fn(ur3wrist1_filepath)
        ur3wrist2_model = base.pg.loadstlaspandanp_fn(ur3wrist2_filepath)
        ur3wrist3_model = base.pg.loadstlaspandanp_fn(ur3wrist3_filepath)

        self.__robotwaist_nodepath = NodePath("ur3dualbody")
        robotwaist_model.instanceTo(self.__robotwaist_nodepath)
        self.__robotwaist_nodepath.setColor(.3, .3, .3, 1)

        # rgt
        self.__ur3rgtbase_nodepath = NodePath("ur3rgtbase")
        self.__ur3rgtshoulder_nodepath = NodePath("ur3rgtshoulder")
        self.__ur3rgtupperarm_nodepath = NodePath("ur3rgtupperarm")
        self.__ur3rgtforearm_nodepath = NodePath("ur3rgtforearm")
        self.__ur3rgtwrist1_nodepath = NodePath("ur3rgtwrist1")
        self.__ur3rgtwrist2_nodepath = NodePath("ur3rgtwrist2")
        self.__ur3rgtwrist3_nodepath = NodePath("ur3rgtwrist3")
        # base egg coordinates is retracted 89.2 along z to fit parameters
        ur3base_model.instanceTo(self.__ur3rgtbase_nodepath)
        self.__ur3rgtbase_nodepath.setColor(.5, .5, .5, 1)
        ur3shoulder_model.instanceTo(self.__ur3rgtshoulder_nodepath)
        self.__ur3rgtshoulder_nodepath.setColor(.5, .7, .3, 1)
        ur3upperarm_model.instanceTo(self.__ur3rgtupperarm_nodepath)
        self.__ur3rgtupperarm_nodepath.setColor(.5, .5, .5, 1)
        ur3forearm_model.instanceTo(self.__ur3rgtforearm_nodepath)
        self.__ur3rgtforearm_nodepath.setColor(.5, .5, .5, 1)
        ur3wrist1_model.instanceTo(self.__ur3rgtwrist1_nodepath)
        self.__ur3rgtwrist1_nodepath.setColor(.5, .7, .3, 1)
        ur3wrist2_model.instanceTo(self.__ur3rgtwrist2_nodepath)
        self.__ur3rgtwrist2_nodepath.setColor(.5, .5, .5, 1)
        ur3wrist3_model.instanceTo(self.__ur3rgtwrist3_nodepath)
        self.__ur3rgtwrist3_nodepath.setColor(.5, .5, .5, 1)
        self.__ur3rgtbase_nodepath.setTransparency(TransparencyAttrib.MDual)
        self.__ur3rgtshoulder_nodepath.setTransparency(TransparencyAttrib.MDual)
        self.__ur3rgtupperarm_nodepath.setTransparency(TransparencyAttrib.MDual)
        self.__ur3rgtforearm_nodepath.setTransparency(TransparencyAttrib.MDual)
        self.__ur3rgtwrist1_nodepath.setTransparency(TransparencyAttrib.MDual)
        self.__ur3rgtwrist2_nodepath.setTransparency(TransparencyAttrib.MDual)
        self.__ur3rgtwrist3_nodepath.setTransparency(TransparencyAttrib.MDual)
        # lft
        self.__ur3lftbase_nodepath = NodePath("ur3lftbase")
        self.__ur3lftshoulder_nodepath = NodePath("ur3lftshoulder")
        self.__ur3lftupperarm_nodepath = NodePath("ur3lftupperarm")
        self.__ur3lftforearm_nodepath = NodePath("ur3lftforearm")
        self.__ur3lftwrist1_nodepath = NodePath("ur3lftwrist1")
        self.__ur3lftwrist2_nodepath = NodePath("ur3lftwrist2")
        self.__ur3lftwrist3_nodepath = NodePath("ur3lftwrist3")
        # base egg coordinates is retracted 89.2 along z to fit parameters
        ur3base_model.instanceTo(self.__ur3lftbase_nodepath)
        self.__ur3lftbase_nodepath.setColor(.5, .5, .5, 1)
        ur3shoulder_model.instanceTo(self.__ur3lftshoulder_nodepath)
        self.__ur3lftshoulder_nodepath.setColor(.5, .7, .3, 1)
        ur3upperarm_model.instanceTo(self.__ur3lftupperarm_nodepath)
        self.__ur3lftupperarm_nodepath.setColor(.5, .5, .5, 1)
        ur3forearm_model.instanceTo(self.__ur3lftforearm_nodepath)
        self.__ur3lftforearm_nodepath.setColor(.5, .5, .5, 1)
        ur3wrist1_model.instanceTo(self.__ur3lftwrist1_nodepath)
        self.__ur3lftwrist1_nodepath.setColor(.5, .7, .3, 1)
        ur3wrist2_model.instanceTo(self.__ur3lftwrist2_nodepath)
        self.__ur3lftwrist2_nodepath.setColor(.5, .5, .5, 1)
        ur3wrist3_model.instanceTo(self.__ur3lftwrist3_nodepath)
        self.__ur3lftwrist3_nodepath.setColor(.5, .5, .5, 1)
        self.__ur3lftbase_nodepath.setTransparency(TransparencyAttrib.MDual)
        self.__ur3lftshoulder_nodepath.setTransparency(TransparencyAttrib.MDual)
        self.__ur3lftupperarm_nodepath.setTransparency(TransparencyAttrib.MDual)
        self.__ur3lftforearm_nodepath.setTransparency(TransparencyAttrib.MDual)
        self.__ur3lftwrist1_nodepath.setTransparency(TransparencyAttrib.MDual)
        self.__ur3lftwrist2_nodepath.setTransparency(TransparencyAttrib.MDual)
        self.__ur3lftwrist3_nodepath.setTransparency(TransparencyAttrib.MDual)

    def gensnp(self, robot, rgtrgba=np.array([.5 ,0 ,0 ,0]), lftrgba=np.array([.5 ,0 ,0 ,0]), drawhand = False,
               toggleendcoord = True, togglejntscoord = False, name = 'robotstick'):
        """
        generate the stick model of ur3
        snp means stick nodepath

        :param robot: the ur3dualrobot
        :param rgtrgba, lftrgba: color of the arm
        :param toggleendcoord: whether to plot end coordinate systems
        :param togglejntscoord: whether to plot joints coordinate systems
        :return: null

        author: weiwei
        date: 20171213
        """

        ur3dualstick = NodePath(name)

        i = 0
        while i != -1:
            base.pggen.plotDumbbell(ur3dualstick, spos=robot.rgtarm[i]['linkpos'], epos=robot.rgtarm[i]['linkend'],
                            thickness=20.0, rgba=rgtrgba)
            i = robot.rgtarm[i]['child']
        i = 0
        while i != -1:
            base.pggen.plotDumbbell(ur3dualstick, spos=robot.lftarm[i]['linkpos'], epos=robot.lftarm[i]['linkend'],
                            thickness=20.0, rgba=lftrgba)
            i = robot.lftarm[i]['child']

        # endcoord
        if toggleendcoord:
            base.pggen.plotAxis(ur3dualstick,
                            spos = robot.rgtarm[-1]['linkend'],
                            pandamat3 = base.pg.npToMat3(robot.rgtarm[-1]['rotmat']), length=100)
            base.pggen.plotAxis(ur3dualstick,
                            spos = robot.lftarm[-1]['linkend'],
                            pandamat3 = base.pg.npToMat3(robot.lftarm[-1]['rotmat']), length=100)
        # toggle all coord
        if togglejntscoord:
            for i in range(1, 7):
                base.pggen.plotAxis(ur3dualstick, spos=robot.rgtarm[i]['linkpos'],
                                pandamat3=base.pg.npToMat3(robot.rgtarm[i]['refcs']), length=100)
            for i in range(1, 7):
                base.pggen.plotAxis(ur3dualstick, spos = robot.lftarm[i]['linkpos'],
                                pandamat3 = base.pg.npToMat3(robot.lftarm[i]['refcs']), length=100)

        # hand
        if robot.rgthnd is not None and drawhand:
            rgthnd = robot.rgthnd.copy()
            rgthnd.setMat(pg.npToMat4(robot.rgtarm[-1]['rotmat'], robot.rgtarm[-1]['linkpos']))
            rgthnd.reparentTo(ur3dualstick)
        else:
            sticknp = base.pggen.genDumbbell(spos=robot.rgtarm[-1]['linkpos'],
                                             epos=robot.rgtarm[-1]['linkend'],
                                             thickness=20, rgba=lftrgba)
            sticknp.reparentTo(ur3dualstick)
        if robot.lfthnd is not None and drawhand:
            lfthnd = robot.lfthnd.copy()
            lfthnd.setMat(pg.npToMat4(robot.lftarm[-1]['rotmat'], robot.lftarm[-1]['linkpos']))
            lfthnd.reparentTo(ur3dualstick)
        else:
            sticknp = base.pggen.genDumbbell(spos=robot.lftarm[-1]['linkpos'],
                                             epos=robot.lftarm[-1]['linkend'],
                                             thickness=20, rgba=lftrgba)
            sticknp.reparentTo(ur3dualstick)

        return ur3dualstick

    def genesnp(self, robot, rgtrgba=None, lftrgba=None, name = 'eesphere'):
        """
        generate the stick model of ur3
        only the end sphere (es) is drawn to show the trajectory of the end effector

        :param robot: the ur3dualrobot
        :param rbga: color of the arm
        :return: null

        author: weiwei
        date: 20181003, madrid
        """

        eesphere = NodePath(name)
        if rgtrgba is not None:
            base.pggen.plotSphere(eesphere, pos=robot.rgtarm[-1]['linkend'], radius=25.0, rgba=rgtrgba)
        if lftrgba is not None:
            base.pggen.plotSphere(eesphere, pos=robot.lftarm[-1]['linkend'], radius=25.0, rgba=lftrgba)
        return eesphere

    def genmnp(self, robot, toggleendcoord = True, togglejntscoord = False, name = 'robotmesh', drawhand = True, rgbargt=None, rgbalft=None):
        """
        generate the mesh model of ur5
        mnp means mesh nodepath

        :param robotjoints: the joint positions of ur5
        :param toggleendcoord: whether to plot end coordinate systems
        :param togglejntscoord: whether to plot joints coordinate systems
        :param rgbargt, rgbalft: the left and right arm color
        :param drawhand: whether to draw hand models
        :return: a nodepath which is ready to be plotted using plotmesh

        author: weiwei
        date: 20180130, 20190527osaka
        """

        self.__robotmesh_nodepath = NodePath(name)
        # robotmesh_nodepath.setMat(base.pg.npToMat4(robot.base['rotmat'], robot.base['linkpos']))

        robotwaist_rotmat = base.pg.npToMat4(robot.base['rotmat'], npvec3=robot.base['linkpos'])
        self.__robotwaist_nodepath.setMat(robotwaist_rotmat)

        # rgt
        # base
        ur3rgtbase_rotmat = base.pg.npToMat4(robot.rgtarm[1]['rotmat'], robot.rgtarm[1]['linkpos'])
        self.__ur3rgtbase_nodepath.setMat(ur3rgtbase_rotmat)
        # shoulder
        ur3rgtshoulder_rotmat = base.pg.npToMat4(robot.rgtarm[1]['rotmat'], robot.rgtarm[1]['linkpos'])
        self.__ur3rgtshoulder_nodepath.setMat(ur3rgtshoulder_rotmat)
        # upperarm
        ur3rgtupperarm_rotmat = base.pg.npToMat4(robot.rgtarm[2]['rotmat'], robot.rgtarm[2]['linkpos'])
        self.__ur3rgtupperarm_nodepath.setMat(ur3rgtupperarm_rotmat)
        # forearm
        ur3rgtforearm_rotmat = base.pg.npToMat4(robot.rgtarm[3]['rotmat'], robot.rgtarm[3]['linkpos'])
        self.__ur3rgtforearm_nodepath.setMat(ur3rgtforearm_rotmat)
        # wrist1
        ur3rgtwrist1_rotmat = base.pg.npToMat4(robot.rgtarm[4]['rotmat'], robot.rgtarm[4]['linkpos'])
        self.__ur3rgtwrist1_nodepath.setMat(ur3rgtwrist1_rotmat)
        # wrist2
        ur3rgtwrist2_rotmat = base.pg.npToMat4(robot.rgtarm[5]['rotmat'], robot.rgtarm[5]['linkpos'])
        self.__ur3rgtwrist2_nodepath.setMat(ur3rgtwrist2_rotmat)
        # wrist3
        ur3rgtwrist3_rotmat = base.pg.npToMat4(robot.rgtarm[6]['rotmat'], robot.rgtarm[6]['linkpos'])
        self.__ur3rgtwrist3_nodepath.setMat(ur3rgtwrist3_rotmat)

        self.__robotwaist_nodepath.reparentTo(self.__robotmesh_nodepath)
        self.__ur3rgtbase_nodepath.reparentTo(self.__robotmesh_nodepath)
        self.__ur3rgtshoulder_nodepath.reparentTo(self.__robotmesh_nodepath)
        self.__ur3rgtupperarm_nodepath.reparentTo(self.__robotmesh_nodepath)
        self.__ur3rgtforearm_nodepath.reparentTo(self.__robotmesh_nodepath)
        self.__ur3rgtwrist1_nodepath.reparentTo(self.__robotmesh_nodepath)
        self.__ur3rgtwrist2_nodepath.reparentTo(self.__robotmesh_nodepath)
        self.__ur3rgtwrist3_nodepath.reparentTo(self.__robotmesh_nodepath)



        alphargt = None
        if rgbargt is not None:
            alphargt = rgbargt[3]
        # endcoord
        if toggleendcoord:
            base.pggen.plotAxis(self.__robotmesh_nodepath,
                            spos = robot.rgtarm[-1]['linkend'],
                            pandamat3 = base.pg.npToMat3(robot.rgtarm[-1]['rotmat']), length=100, alpha=alphargt)
        # toggle all coord
        if togglejntscoord:
            for i in range(1, 7):
                base.pggen.plotAxis(self.__robotmesh_nodepath, spos=robot.rgtarm[i]['linkpos'],
                                pandamat3=base.pg.npToMat3(robot.rgtarm[i]['refcs']), length=100, alpha=alphargt)

        # hand
        if robot.rgthnd is not None and drawhand:
            rgthnd = robot.rgthnd.copy()
            if rgbargt is not None:
                self.__setRgtColor(rgbargt[0], rgbargt[1], rgbargt[2], rgbargt[3])
                rgthnd.setColor(rgbargt[0], rgbargt[1], rgbargt[2], rgbargt[3])
            else:
                self.__setRgtDefaultColor()
                rgthnd.setDefaultColor()
            rgthnd.setMat(base.pg.npToMat4(robot.rgtarm[-1]['rotmat'], robot.rgtarm[-1]['linkpos']))
            rgthnd.reparentTo(self.__robotmesh_nodepath)
 
        #lft
        # base
        ur3lftbase_rotmat = base.pg.npToMat4(robot.lftarm[1]['rotmat'], robot.lftarm[1]['linkpos'])
        self.__ur3lftbase_nodepath.setMat(ur3lftbase_rotmat)
        # shoulder
        ur3lftshoulder_rotmat = base.pg.npToMat4(robot.lftarm[1]['rotmat'], robot.lftarm[1]['linkpos'])
        self.__ur3lftshoulder_nodepath.setMat(ur3lftshoulder_rotmat)
        # upperarm
        ur3lftupperarm_rotmat = base.pg.npToMat4(robot.lftarm[2]['rotmat'], robot.lftarm[2]['linkpos'])
        self.__ur3lftupperarm_nodepath.setMat(ur3lftupperarm_rotmat)
        # forearm
        ur3lftforearm_rotmat = base.pg.npToMat4(robot.lftarm[3]['rotmat'], robot.lftarm[3]['linkpos'])
        self.__ur3lftforearm_nodepath.setMat(ur3lftforearm_rotmat)
        # wrist1
        ur3lftwrist1_rotmat = base.pg.npToMat4(robot.lftarm[4]['rotmat'], robot.lftarm[4]['linkpos'])
        self.__ur3lftwrist1_nodepath.setMat(ur3lftwrist1_rotmat)
        # wrist2
        ur3lftwrist2_rotmat = base.pg.npToMat4(robot.lftarm[5]['rotmat'], robot.lftarm[5]['linkpos'])
        self.__ur3lftwrist2_nodepath.setMat(ur3lftwrist2_rotmat)
        # wrist3
        ur3lftwrist3_rotmat = base.pg.npToMat4(robot.lftarm[6]['rotmat'], robot.lftarm[6]['linkpos'])
        self.__ur3lftwrist3_nodepath.setMat(ur3lftwrist3_rotmat)

        self.__ur3lftbase_nodepath.reparentTo(self.__robotmesh_nodepath)
        self.__ur3lftshoulder_nodepath.reparentTo(self.__robotmesh_nodepath)
        self.__ur3lftupperarm_nodepath.reparentTo(self.__robotmesh_nodepath)
        self.__ur3lftforearm_nodepath.reparentTo(self.__robotmesh_nodepath)
        self.__ur3lftwrist1_nodepath.reparentTo(self.__robotmesh_nodepath)
        self.__ur3lftwrist2_nodepath.reparentTo(self.__robotmesh_nodepath)
        self.__ur3lftwrist3_nodepath.reparentTo(self.__robotmesh_nodepath)

        alphalft = None
        if rgbalft is not None:
            alphalft = rgbalft[3]
        # endcoord
        if toggleendcoord:
            base.pggen.plotAxis(self.__robotmesh_nodepath,
                            spos = robot.lftarm[-1]['linkend'],
                            pandamat3 = base.pg.npToMat3(robot.lftarm[-1]['rotmat']), length=100, alpha=alphalft)
        # toggle all coord
        if togglejntscoord:
            for i in range(1, 7):
                base.pggen.plotAxis(self.__robotmesh_nodepath, spos = robot.lftarm[i]['linkpos'],
                                pandamat3 = base.pg.npToMat3(robot.lftarm[i]['refcs']), length=100, alpha=alphalft)

        # hand
        if robot.lfthnd is not None and drawhand:
            lfthnd = robot.lfthnd.copy()
            if rgbalft is not None:
                self.__setLftColor(rgbalft[0], rgbalft[1], rgbalft[2], rgbalft[3])
                lfthnd.setColor(rgbalft[0], rgbalft[1], rgbalft[2], rgbalft[3])
            else:
                self.__setLftDefaultColor()
                lfthnd.setDefaultColor()
            lfthnd.setMat(base.pg.npToMat4(robot.lftarm[-1]['rotmat'], robot.lftarm[-1]['linkpos']))
            lfthnd.reparentTo(self.__robotmesh_nodepath)

        return copy.deepcopy(self.__robotmesh_nodepath)

    def __setRgtDefaultColor(self):
        self.__ur3rgtbase_nodepath.setColor(.5,.5,.5,1)
        self.__ur3rgtshoulder_nodepath.setColor(.1,.3,.5,1)
        self.__ur3rgtupperarm_nodepath.setColor(.7,.7,.7,1)
        self.__ur3rgtforearm_nodepath.setColor(.35,.35,.35,1)
        self.__ur3rgtwrist1_nodepath.setColor(.7,.7,.7,1)
        self.__ur3rgtwrist2_nodepath.setColor(.1,.3,.5,1)
        self.__ur3rgtwrist3_nodepath.setColor(.5,.5,.5,1)

    def __setLftDefaultColor(self):
        self.__ur3lftbase_nodepath.setColor(.5,.5,.5,1)
        self.__ur3lftshoulder_nodepath.setColor(.1,.3,.5,1)
        self.__ur3lftupperarm_nodepath.setColor(.7,.7,.7,1)
        self.__ur3lftforearm_nodepath.setColor(.35,.35,.35,1)
        self.__ur3lftwrist1_nodepath.setColor(.7,.7,.7,1)
        self.__ur3lftwrist2_nodepath.setColor(.1,.3,.5,1)
        self.__ur3lftwrist3_nodepath.setColor(.5,.5,.5,1)

    def __setRgtColor(self, *args, **kwargs):
        """

        :param rgba
        :return:

        author: weiwei
        date: 20190527
        """

        self.__ur3rgtbase_nodepath.clearColor()
        self.__ur3rgtshoulder_nodepath.clearColor()
        self.__ur3rgtupperarm_nodepath.clearColor()
        self.__ur3rgtforearm_nodepath.clearColor()
        self.__ur3rgtwrist1_nodepath.clearColor()
        self.__ur3rgtwrist2_nodepath.clearColor()
        self.__ur3rgtwrist3_nodepath.clearColor()
        if len(args)==1:
            self.__ur3rgtbase_nodepath.setColor(args[0])
            self.__ur3rgtshoulder_nodepath.setColor(args[0])
            self.__ur3rgtupperarm_nodepath.setColor(args[0])
            self.__ur3rgtforearm_nodepath.setColor(args[0])
            self.__ur3rgtwrist1_nodepath.setColor(args[0])
            self.__ur3rgtwrist2_nodepath.setColor(args[0])
            self.__ur3rgtwrist3_nodepath.setColor(args[0])
        else:
            self.__ur3rgtbase_nodepath.setColor(args[0], args[1], args[2], args[3])
            self.__ur3rgtshoulder_nodepath.setColor(args[0], args[1], args[2], args[3])
            self.__ur3rgtupperarm_nodepath.setColor(args[0], args[1], args[2], args[3])
            self.__ur3rgtforearm_nodepath.setColor(args[0], args[1], args[2], args[3])
            self.__ur3rgtwrist1_nodepath.setColor(args[0], args[1], args[2], args[3])
            self.__ur3rgtwrist2_nodepath.setColor(args[0], args[1], args[2], args[3])
            self.__ur3rgtwrist3_nodepath.setColor(args[0], args[1], args[2], args[3])

    def __setLftColor(self, *args, **kwargs):
        """

        :param rgba
        :return:

        author: weiwei
        date: 20190527
        """

        self.__ur3lftbase_nodepath.clearColor()
        self.__ur3lftshoulder_nodepath.clearColor()
        self.__ur3lftupperarm_nodepath.clearColor()
        self.__ur3lftforearm_nodepath.clearColor()
        self.__ur3lftwrist1_nodepath.clearColor()
        self.__ur3lftwrist2_nodepath.clearColor()
        self.__ur3lftwrist3_nodepath.clearColor()
        if len(args)==1:
            self.__ur3lftbase_nodepath.setColor(args[0])
            self.__ur3lftshoulder_nodepath.setColor(args[0])
            self.__ur3lftupperarm_nodepath.setColor(args[0])
            self.__ur3lftforearm_nodepath.setColor(args[0])
            self.__ur3lftwrist1_nodepath.setColor(args[0])
            self.__ur3lftwrist2_nodepath.setColor(args[0])
            self.__ur3lftwrist3_nodepath.setColor(args[0])
        else:
            self.__ur3lftbase_nodepath.setColor(args[0], args[1], args[2], args[3])
            self.__ur3lftshoulder_nodepath.setColor(args[0], args[1], args[2], args[3])
            self.__ur3lftupperarm_nodepath.setColor(args[0], args[1], args[2], args[3])
            self.__ur3lftforearm_nodepath.setColor(args[0], args[1], args[2], args[3])
            self.__ur3lftwrist1_nodepath.setColor(args[0], args[1], args[2], args[3])
            self.__ur3lftwrist2_nodepath.setColor(args[0], args[1], args[2], args[3])
            self.__ur3lftwrist3_nodepath.setColor(args[0], args[1], args[2], args[3])


if __name__=="__main__":

    # show in panda3d
    from direct.showbase.ShowBase import ShowBase
    from panda3d.core import *
    from panda3d.bullet import BulletSphereShape
    import pandaplotutils.pandageom as pandageom
    import pandaplotutils.pandactrl as pandactrl
    from direct.filter.CommonFilters import CommonFilters
    import ur3dual

    base = pandactrl.World(camp = [3000,0,3000], lookatp = [0,0,700])

    ur3dualrobot = ur3dual.Ur3DualRobot()
    ur3dualrobot.goinitpose()
    # ur3dualrobot.gozeropose()
    ur3dualrobotmesh = Ur3DualMesh()
    ur3meshnm = ur3dualrobotmesh.genmnp(ur3dualrobot, togglejntscoord=True)
    ur3meshnm.reparentTo(base.render)
    ur3snp = ur3dualrobotmesh.gensnp(ur3dualrobot)
    ur3snp.reparentTo(base.render)

    base.run()
